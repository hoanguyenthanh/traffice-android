package com.htn.traffic.handledata.xmlhandle;

import android.content.res.XmlResourceParser;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.htn.traffic.R;
import com.htn.traffic.UIApplication;
import com.htn.traffic.helpers.Utilities;
import com.htn.traffic.models.Choice;
import com.htn.traffic.models.Question;
import com.htn.traffic.models.Topic;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

public class ImportData {
	private SQLiteDatabase db;
	public ImportData(SQLiteDatabase db) {
		this.db = db;
	}

	/**
	 * Import data from res/xml/questions.xml
	 */
	public void importDataFromResource() {
		ReadXMLHelper helper = new ReadXMLHelper();
        List<XmlResourceParser> listXmlResourceParser = new ArrayList<XmlResourceParser>();

        XmlResourceParser xmlRPGroup1 = UIApplication.getContext().getResources().getXml(R.xml.grp1_khai_niem_va_quy_tac_145);
        XmlResourceParser xmlRPGroup2 = UIApplication.getContext().getResources().getXml(R.xml.grp2_nghiep_vu_van_tai_30);
        XmlResourceParser xmlRPGroup3 = UIApplication.getContext().getResources().getXml(R.xml.grp3_van_hoa_dao_duc_nguoi_lai_xe_25);
        XmlResourceParser xmlRPGroup4 = UIApplication.getContext().getResources().getXml(R.xml.grp4_ky_thuat_lai_xe_oto_35);
        XmlResourceParser xmlRPGroup5 = UIApplication.getContext().getResources().getXml(R.xml.grp5_cau_tao_va_sua_chua_oto_20);
        XmlResourceParser xmlRPGroup6 = UIApplication.getContext().getResources().getXml(R.xml.grp6_he_thong_bien_bao_100);
        XmlResourceParser xmlRPGroup7 = UIApplication.getContext().getResources().getXml(R.xml.grp7_sa_hinh_95);

        listXmlResourceParser.add(xmlRPGroup1);
        listXmlResourceParser.add(xmlRPGroup2);
        listXmlResourceParser.add(xmlRPGroup3);
        listXmlResourceParser.add(xmlRPGroup4);
        listXmlResourceParser.add(xmlRPGroup5);
        listXmlResourceParser.add(xmlRPGroup6);
        listXmlResourceParser.add(xmlRPGroup7);

        for (XmlResourceParser xmlResourceParser : listXmlResourceParser) {
            helper.readXML(UIApplication.getContext(), xmlResourceParser);

            Collection<Topic> topicList = helper.getTopicList();
            Collection<Question> questionList = helper.getQuestionList();
            Collection<Choice> choiceList = helper.getChoiceList();

			insertData(topicList, questionList, choiceList);
        }

        listXmlResourceParser.clear();
	}

	/**
	 * Import data from an URL e.g
	 * https://dl.dropbox.com/u/7059745/questions.xml
	 */
	public boolean importDataFromURL(String strURL) {
		boolean flag = true;
		InputStream in = Utilities.checkFileAvailableStatus(strURL);

		if (in != null) {
			Collection<Topic> topicList = null;
			Collection<Question> questionList = null;
			Collection<Choice> choiceList = null;
			QuestionXMLHandler handler = new QuestionXMLHandler();
			XMLReader xmlreader;
			try {
				xmlreader = ReadXMLHelper.initializeReader();
				xmlreader.setContentHandler(handler);
				xmlreader.parse(new InputSource(in));
			} catch (ParserConfigurationException e) {
				flag = false;
			} catch (SAXException e) {
				flag = false;
			} catch (IOException e) {
				flag = false;
			}
			topicList = handler.getTopicList();
			questionList = handler.getQuestionList();
			choiceList = handler.getChoiceList();

			this.insertData(topicList, questionList, choiceList);
		} else {
			flag = false;
		}
		return flag;
	}

	public void insertData(Collection<Topic> topicList, Collection<Question> questionList, Collection<Choice> choiceList) {
		Log.i("ImportData", "importData()");
		// ca 3 list phai !=null
		if (topicList != null && questionList != null && choiceList != null) {
			// unlock db
			db.setLockingEnabled(false);

			// begin Transaction
			db.beginTransaction();

			// TOPIC
			// Create a single InsertHelper to handle this set of insertions.
			InsertHelper topicInsertHelper = new InsertHelper(db, DBHelper.TOPIC_TABLE_NAME);
			// Get the numeric indexes for each of the columns that we're updating
			final int iTopicTitle = topicInsertHelper.getColumnIndex(DBHelper.TOPIC_TITLE);
			final int iCategory = topicInsertHelper.getColumnIndex(DBHelper.TOPIC_CATEGORY);
			final int iDescription = topicInsertHelper.getColumnIndex(DBHelper.TOPIC_DESCRIPTION);

			//QUESTION
			// Create a single InsertHelper to handle this set of insertions.
			InsertHelper questionInsertHelper = new InsertHelper(db,DBHelper.QUESTION_TABLE_NAME);
			// Get the numeric indexes for each of the columns that we're updating
			final int iSortOrder = questionInsertHelper.getColumnIndex(DBHelper.QUESTION_SORT_ORDER);
			final int iQuestionTitle = questionInsertHelper.getColumnIndex(DBHelper.QUESTION_TITLE);
			final int iQuestionImage = questionInsertHelper.getColumnIndex(DBHelper.QUESTION_IMAGE);
			final int iTopic = questionInsertHelper.getColumnIndex(DBHelper.QUESTION_TOPIC);
			final int iE150 = questionInsertHelper.getColumnIndex(DBHelper.QUESTION_E150);

			// CHOICE
			// Create a single InsertHelper to handle this set of insertions.
			InsertHelper choiceInsertHelper = new InsertHelper(db,DBHelper.CHOICE_TABLE_NAME);

			// Get the numeric indexes for each of the columns that we're updating
			final int iChoiceTitle = choiceInsertHelper.getColumnIndex(DBHelper.CHOICE_TITLE);
			final int iIsCorrect = choiceInsertHelper.getColumnIndex(DBHelper.CHOICE_IS_CORRECT);
			final int iQuestID = choiceInsertHelper.getColumnIndex(DBHelper.CHOICE_QUESTION_ID);

			int rowsTopic = 0;
			int rowsQuestion = 0;
			int rowsChoice = 0;
			long topicID = 0;
			long questionID = 0;

			try {
				for (Topic topic : topicList) {
					topicInsertHelper.prepareForInsert();
					topicInsertHelper.bind(iTopicTitle, topic.getTitle());
					topicInsertHelper.bind(iDescription, topic.getDescription());
					topicInsertHelper.bind(iCategory, topic.getCategory());

					// EXECUTE, return -1 if fail
					topicID = topicInsertHelper.execute();

					if (topicID != -1) {
						rowsTopic++;
						for (Question question : questionList) {
							if (question.topic == topic.getId()) {
								questionInsertHelper.prepareForInsert();
								questionInsertHelper.bind(iSortOrder, question.sortOrder);
								questionInsertHelper.bind(iTopic, topicID);
								questionInsertHelper.bind(iQuestionTitle, question.title);
								questionInsertHelper.bind(iQuestionImage, question.image);
								questionInsertHelper.bind(iE150, question.e150);

								// EXECUTE, return -1 if fail
								questionID = questionInsertHelper.execute();
								if (questionID != -1) {
									Log.i("InsertQuest", "COMPLETE:" + Long.toString(questionID));
									rowsQuestion++;
									for (Choice choice : choiceList) {
										if (choice.getQuestion_id() == question.questId) {
											choiceInsertHelper.prepareForInsert();
											choiceInsertHelper.bind(iChoiceTitle, choice.getTitle());
											choiceInsertHelper.bind(iIsCorrect,choice.getIsCorrect());
											choiceInsertHelper.bind(iQuestID, questionID);
											if (choiceInsertHelper.execute() != -1) {
												rowsChoice++;
												Log.i("InsertChoice", "COMPLETE" + rowsChoice);
											}else{
												Log.i("InsertChoice", "ERROR");
											}
										}
									}
								}else{
									Log.i("InsertQuest", "ERROR");
								}
							}
						}
					}
				}
				// COMMIT
				db.setTransactionSuccessful();
			} finally {
				// end Transaction
				db.endTransaction();
				// lock db as default
				db.setLockingEnabled(true);
				choiceInsertHelper.close();
				questionInsertHelper.close();
				topicInsertHelper.close();
				// db.close();
			}
			Log.i("ImportData", rowsTopic + "/" + topicList.size() + " topics created");
			Log.i("ImportData", rowsQuestion + "/" + questionList.size() + " questions created");
			Log.i("ImportData", rowsChoice + "/" + choiceList.size() + " choices created");
		}
	}
}
