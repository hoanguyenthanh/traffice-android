package com.htn.traffic.handledata;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.htn.traffic.handledata.xmlhandle.DBHelper;
import com.htn.traffic.models.Choice;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;


public class ChoiceDAL {
	private SQLiteDatabase db;

	public ChoiceDAL(SQLiteDatabase db) {
		this.db = db;
	}

	public long insert(Choice choice) {
		return db.insert(DBHelper.CHOICE_TABLE_NAME, null, this.getContentValues(choice));
	}

	public long update(Choice choice) {
		return db.update(DBHelper.CHOICE_TABLE_NAME
				, this.getContentValues(choice)
				, "rowid = ?"
				, new String[] { "" + choice.getId() });
	}

	public int delete(Long choiceId) {
		return db.delete(DBHelper.CHOICE_TABLE_NAME, "_id = ?",
				new String[] { choiceId.toString() });
	}

	public Choice getChoice(int choiceId) {
		Choice choice = new Choice();
		Cursor cursor = db.query(DBHelper.CHOICE_TABLE_NAME
				, DBHelper.CHOICE_COLUMNS
				, "_id = ?"
				, new String[] { "" + choiceId }
				, null, null, null);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			choice = convertCursor2Choice(cursor);
		}

		if (cursor != null) {
			cursor.close();
		}
		return choice;
	}

	public List<Choice> getChoices(int questId) {
		List<Choice> list = new ArrayList<Choice>();

		Cursor cursor = db.query(DBHelper.CHOICE_TABLE_NAME
				, DBHelper.CHOICE_COLUMNS
				, DBHelper.CHOICE_QUESTION_ID + " = ?"
				, new String[] { "" + questId }
				, null, null, null);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Choice(cursor));
		}

		if (cursor != null) {
			cursor.close();
		}
		return list;
	}

	public List<Choice> getChoices(List<Question> listQuest) {
		List<Choice> list = new ArrayList<Choice>();
		int size = listQuest.size();

		StringBuilder buff = new StringBuilder(" IN (");
		if (size > 1) {
			for (int i = 0; i < size; i++) {
				buff.append(", ?");
			}
		} else { // size = 1
			buff.append("?");
		}
		buff.append(") ");

		String[] ids = new String[size];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = listQuest.get(i).questId + "";
		}

		Cursor cursor = db.query(DBHelper.CHOICE_TABLE_NAME
				, DBHelper.CHOICE_COLUMNS
				, DBHelper.CHOICE_QUESTION_ID + buff.toString(), ids
				, null, null, null);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Choice(cursor));
		}

		if (cursor != null) {
			cursor.close();
		}
		return list;
	}

	private Choice convertCursor2Choice(Cursor cursor) {
		Choice choice = new Choice();
		choice.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.CHOICE_ID)));
		choice.setTitle(cursor.getString(cursor
				.getColumnIndex(DBHelper.CHOICE_TITLE)));
		choice.setIsCorrect(cursor.getInt(cursor
				.getColumnIndex(DBHelper.CHOICE_IS_CORRECT)));
		choice.setQuestion_id(cursor.getInt(cursor
				.getColumnIndex(DBHelper.CHOICE_QUESTION_ID)));
		return choice;
	}


	public ContentValues getContentValues(Choice choice) {
		ContentValues values = new ContentValues();
		// id tu tang nen ko can add zo
		values.put(DBHelper.CHOICE_TITLE, choice.getTitle());
		values.put(DBHelper.CHOICE_IS_CORRECT, choice.getIsCorrect());
		values.put(DBHelper.CHOICE_QUESTION_ID, choice.getQuestion_id());
		return values;
	}

	public void removeDuplicates() {
		String sql = "delete from " + DBHelper.CHOICE_TABLE_NAME
				+ " where rowid in ( " + "select rowid from "
				+ DBHelper.CHOICE_TABLE_NAME + " as Duplicates where rowid > "
				+ "(select min(rowid) from " + DBHelper.CHOICE_TABLE_NAME
				+ " as First where First." + DBHelper.CHOICE_TITLE
				+ " = Duplicates." + DBHelper.CHOICE_TITLE + " GROUP BY First."
				+ DBHelper.CHOICE_QUESTION_ID + "));";

		db.execSQL(sql);

		sql = null;
	}
}
