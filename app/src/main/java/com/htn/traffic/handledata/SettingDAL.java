package com.htn.traffic.handledata;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.htn.traffic.models.Setting;

public class SettingDAL {
	Context context;
	public static final int MODE = Context.MODE_PRIVATE;
	public static final String PREF_NAME = "SettingManager";
	public static final String SETTING_TIMER = "timer";
	public static final String SETTING_HIDE_IS_CORRECT = "hideIsCorrect";
	public static final String SETTING_RANDOM_QUESTION = "isRandomQuestion";


	public SettingDAL(Context context) {
		this.context = context;
	}

	private Editor getEditor() {
		return this.context.getSharedPreferences(PREF_NAME, MODE).edit();
	}

	/**
	 * Luu tru cac thong so cau hinh
	 * 
	 * @param setting
	 * @return true neu nhu save thanh cong
	 */
	public boolean setSetting(Setting setting) {
		Editor editor = this.getEditor();
		editor.putBoolean(SETTING_TIMER, setting.getTimer());
		editor.putBoolean(SETTING_HIDE_IS_CORRECT,setting.getHideIsCorrect());
		editor.putBoolean(SETTING_RANDOM_QUESTION, setting.getRandomQuestion());
		return editor.commit();
	}

	public Setting getSetting() {
		Setting setting = new Setting();
		SharedPreferences pref = this.context.getSharedPreferences(PREF_NAME,MODE);
			setting.setTimer(pref.getBoolean(SETTING_TIMER, false));
			setting.setHideIsCorrect(pref.getBoolean(SETTING_HIDE_IS_CORRECT, false));
			setting.setRandomQuestion(pref.getBoolean(SETTING_RANDOM_QUESTION, false));
		return setting;
	}
	/**
	 * @return Default setting after reset
	 */
	public Setting reset() {
		Setting setting = new Setting();
		Editor editor = this.getEditor();
		editor.putBoolean(SETTING_TIMER, setting.getTimer());
		editor.putBoolean(SETTING_HIDE_IS_CORRECT,setting.getHideIsCorrect());
		editor.putBoolean(SETTING_RANDOM_QUESTION, setting.getRandomQuestion());
		editor.commit();
		return setting;
	}

	/**
	 * Remove all values of Pref
	 */
	public void clear() {
		this.context.getSharedPreferences(PREF_NAME, MODE).edit().clear().commit();
	}
}
