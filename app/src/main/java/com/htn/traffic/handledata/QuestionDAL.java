package com.htn.traffic.handledata;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.htn.traffic.handledata.xmlhandle.DBHelper;
import com.htn.traffic.helpers.Constants;
import com.htn.traffic.helpers.Utilities;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;


public class QuestionDAL {
	private SQLiteDatabase db;

	public QuestionDAL(SQLiteDatabase db) {
        this.db = db;
	}

	public long insert(Question question) {
		return db.insert(DBHelper.QUESTION_TABLE_NAME, null, this.getContentValues(question));
	}

	public long update(Question question) {
		return db.update(DBHelper.QUESTION_TABLE_NAME
				, this.getContentValues(question)
				, "_id = ?"
				, new String[] { "" + question.questId });
	}

	public int delete(Integer id) {
		return db.delete(DBHelper.QUESTION_TABLE_NAME
				, "_id = ?"
				, new String[]{id.toString()});
	}

	public int updateQuestMarked(int questId, int flagMark){
		ContentValues values = new ContentValues();
		values.put(DBHelper.QUESTION_IS_MARK, flagMark);

		return db.update(DBHelper.QUESTION_TABLE_NAME
				, values
				, "_id = ?"
				, new String[] { "" + questId});
	}

	public List<Question> getQuestions(int topic, int groupTopicId){
		List<Question> list = new ArrayList<Question>();
		Cursor cursor = null;
		if (groupTopicId == Constants.TOPIC_450) {
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS
					, DBHelper.QUESTION_TOPIC + " = ?"
					, new String[]{Integer.toString(topic)}, null, null, null);
		}else if(groupTopicId == Constants.TOPIC_150){
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS
					, DBHelper.QUESTION_TOPIC + " = ? AND e150 > 0 "
					, new String[]{Integer.toString(topic)}, null, null, null);
		}
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Question(cursor));
		}

		if (cursor != null) {
			cursor.close();
		}
		return list;
	}

	public List<Question> getQuestionsRandom(int topic, int groupTopicId, int limit) {
		List<Question> list = new ArrayList<Question>();
		Cursor cursor = null;

		if (groupTopicId == Constants.TOPIC_450) {
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS
					, DBHelper.QUESTION_TOPIC + " = ?"
					, new String[]{"" + topic}
					, null, null, "RANDOM() LIMIT " + limit);
		}else if(groupTopicId == Constants.TOPIC_150){
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS
					, DBHelper.QUESTION_TOPIC + " = ? AND e150 > 0 "
					, new String[]{"" + topic}
					, null, null, " RANDOM() LIMIT " + limit);
		}

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Question(cursor));
		}

		if (cursor != null) {
			cursor.close();
		}

		return list;
	}

	public List<Question> getQuestionsMarked(int groupTopicId) {
		List<Question> list = new ArrayList<Question>();
		Cursor cursor = null;
		if (groupTopicId == Constants.TOPIC_450) {
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS
					, DBHelper.QUESTION_IS_MARK + " = 1"
					, null, null, null, null);
		} else if (groupTopicId == Constants.TOPIC_150){
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS
					, DBHelper.QUESTION_IS_MARK + " = 1 AND e150 > 0 "
					, null, null, null, null);
		}

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Question(cursor));
		}

		if (cursor != null) {
			cursor.close();
		}
		return list;
	}

	/**
	 * Get list question by list id
	 * @param questionIds
	 *     Array questionId
	 * @param groupTopicId
	 *     Type exam: 150 or 450
     * @return
	 *     List question
     */
	public List<Question> getQuestions(int[] questionIds, int groupTopicId) {
		List<Question> list = new ArrayList<Question>();
		if (questionIds.length == 0) {
			return list;
		}

		String in = Utilities.convertMultiValues2String(questionIds);

		Cursor cursor = null;
		if (groupTopicId == Constants.TOPIC_450) {
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS, " _id IN " + in, null, null, null, null);
		}else if (groupTopicId == Constants.TOPIC_150){
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS, " e150 > 0 AND _id IN " + in, null, null, null, null);
		}
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Question(cursor));
		}

		cursor.close();
		return list;
	}

	public List<Question> getQuestionsAll(int groupTopicId) {
		List<Question> list = new ArrayList<Question>();
		Cursor cursor = null;
		if (groupTopicId == Constants.TOPIC_450) {
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS, null, null, null, null, null);
		}else{
			cursor = db.query(DBHelper.QUESTION_TABLE_NAME
					, DBHelper.QUESTION_COLUMNS, " e150 > 0 " , null, null, null, null);
		}

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Question(cursor));
		}
		cursor.close();

		return list;
	}

	private Question convertCursor2Question(Cursor cursor) {
		Question question = new Question();
		question.setQuestId(cursor.getInt(0));
		question.setSortOrder(cursor.getInt(1));
		question.setTitle(cursor.getString(2));
		question.setImage(cursor.getString(3));
		question.setIsMark(cursor.getInt(4));
		question.setTopic(cursor.getInt(5));
		question.setE150(cursor.getInt(6));
		question.setE450(cursor.getInt(7));

		return question;
	}

	public ContentValues getContentValues(Question question) {
		ContentValues values = new ContentValues();
		// id
		values.put(DBHelper.QUESTION_TITLE, question.title);
		values.put(DBHelper.QUESTION_IMAGE, question.image);
		values.put(DBHelper.QUESTION_IS_MARK, question.isMark);
		values.put(DBHelper.QUESTION_TOPIC, question.topic);
		return values;
	}
}
