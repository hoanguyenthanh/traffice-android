package com.htn.traffic.handledata;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class HistoryStudyDAL {
	Context context;
	
	public static final int MODE = Context.MODE_PRIVATE;
	public static final String PRE_STUDY = "indexQuestionHistory";
	public static final String LAST_INDEX_ITEM_150 = "LAST_INDEX_ITEM_150";
	public static final String LAST_INDEX_ITEM_450 = "LAST_INDEX_ITEM_450";
	public static final String LAST_INDEX_ITEM_TOPIC01 = "LAST_INDEX_ITEM_TOPIC01";
	public static final String LAST_INDEX_ITEM_TOPIC02 = "LAST_INDEX_ITEM_TOPIC02";
	public static final String LAST_INDEX_ITEM_TOPIC03 = "LAST_INDEX_ITEM_TOPIC03";
	public static final String LAST_INDEX_ITEM_TOPIC04 = "LAST_INDEX_ITEM_TOPIC04";
	public static final String LAST_INDEX_ITEM_TOPIC05 = "LAST_INDEX_ITEM_TOPIC05";
	public static final String LAST_INDEX_ITEM_TOPIC06 = "LAST_INDEX_ITEM_TOPIC06";
	public static final String LAST_INDEX_ITEM_TOPIC07 = "LAST_INDEX_ITEM_TOPIC07";

	public static final String SCORE_150_EXAM01 = "SCORE_150_EXAM01";
	public static final String SCORE_150_EXAM02 = "SCORE_150_EXAM02";
	public static final String SCORE_150_EXAM03 = "SCORE_150_EXAM03";
	public static final String SCORE_150_EXAM04 = "SCORE_150_EXAM04";
	public static final String SCORE_150_EXAM05 = "SCORE_150_EXAM05";
	public static final String SCORE_150_EXAM06 = "SCORE_150_EXAM06";
	public static final String SCORE_150_EXAM07 = "SCORE_150_EXAM07";
	public static final String SCORE_150_EXAM08 = "SCORE_150_EXAM08";

	public static final String SCORE_450_EXAM01 = "SCORE_450_EXAM01";
	public static final String SCORE_450_EXAM02 = "SCORE_450_EXAM02";
	public static final String SCORE_450_EXAM03 = "SCORE_450_EXAM03";
	public static final String SCORE_450_EXAM04 = "SCORE_450_EXAM04";
	public static final String SCORE_450_EXAM05 = "SCORE_450_EXAM05";
	public static final String SCORE_450_EXAM06 = "SCORE_450_EXAM06";
	public static final String SCORE_450_EXAM07 = "SCORE_450_EXAM07";
	public static final String SCORE_450_EXAM08 = "SCORE_450_EXAM08";
	public static final String SCORE_450_EXAM09 = "SCORE_450_EXAM09";
	public static final String SCORE_450_EXAM10 = "SCORE_450_EXAM10";
	public static final String SCORE_450_EXAM11 = "SCORE_450_EXAM11";
	public static final String SCORE_450_EXAM12 = "SCORE_450_EXAM12";
	public static final String SCORE_450_EXAM13 = "SCORE_450_EXAM13";
	public static final String SCORE_450_EXAM14 = "SCORE_450_EXAM14";
	public static final String SCORE_450_EXAM15 = "SCORE_450_EXAM15";


	public HistoryStudyDAL(Context con){
		this.context = con;
	}
	private Editor getEditor(){
		return this.context.getSharedPreferences(PRE_STUDY,MODE).edit();
	}

	public void clearHistoryStudy(){
		this.context.getSharedPreferences(PRE_STUDY, MODE).edit().clear().commit();
	}

	//Tất cả các câu hỏi xe mô tô 150
	public boolean setLastIndexItem150(int index){
		Editor edit = getEditor();
		edit.putInt(LAST_INDEX_ITEM_150, index);
		return edit.commit();
	}

	//Tất cả các câu hỏi xe ô tô 450
	public boolean setLastIndexItem450(int index){
		Editor edit = getEditor();
		edit.putInt(LAST_INDEX_ITEM_450, index);
		return edit.commit();
	}

	//Khái niệm và quy tắc giao thông đường bộ
	public Boolean setLastIndexTopic01(int index) {
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC01, index);
		return edit.commit();
	}

	//Nghiệp vụ vận tải
	public Boolean setLastIndexTopic02(int index) {
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC02, index);
		return edit.commit();
	}

	//Văn hóa, đạo đức nghề nghiệp người lái xe
	public Boolean setLastIndexTopic03(int value) {
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC03, value);
		return edit.commit();
	}

	//Kỹ thuật lái xe ô tô
	public Boolean setLastIndexTopic04(int value) {
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC04, value);
		return edit.commit();
	}

	//Kỹ thuật lái xe ô tô
	public Boolean setLastIndexTopic05(int value){
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC05, value);
		return edit.commit();
	}

	//Hệ thống biển báo hiệu đường bộ
	public Boolean setLastIndexTopic06(int value){
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC06, value);
		return edit.commit();
	}

	//Giải các thế sa hình
	public Boolean setLastIndexTopic07(int value){
		Editor edit = this.getEditor();
		edit.putInt(LAST_INDEX_ITEM_TOPIC07, value);
		return edit.commit();
	}

	//Tất cả các câu hỏi xe mô tô 150
	public int getLastIndexItem150(){
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_150, 0);
	}

	//Tất cả các câu hỏi xe ô tô 450
	public int getLastIndexItem450(){
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_450, 0);
	}

	//Khái niệm và quy tắc giao thông đường bộ
	public int getLastIndexTopic01() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC01, 0);
	}

	//Nghiệp vụ vận tải
	public int getLastIndexTopic02() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC02, 0);

	}

	//Văn hóa, đạo đức nghề nghiệp người lái xe
	public int getLastIndexTopic03() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC03, 0);

	}

	//Kỹ thuật lái xe ô tô
	public int getLastIndexTopic04() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC04, 0);

	}

	//Cấu tạo và sửa chữa xe ô tô
	public int getLastIndexTopic05() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC05, 0);
	}

	//Hệ thống biển báo hiệu đường bộ
	public int getLastIndexTopic06() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC06, 0);
	}

	//Giải các thế sa hình
	public int getLastIndexTopic07() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(LAST_INDEX_ITEM_TOPIC07, 0);
	}

	/******************************************************************************/
	//HISTORY SCORE FOR EXAM 150

	//Set value
	public boolean setScore150Exam01(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM01, val);
		return editor.commit();
	}

	public boolean setScore150Exam02(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM02, val);
		return editor.commit();
	}

	public boolean setScore150Exam03(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM03, val);
		return editor.commit();
	}

	public boolean setScore150Exam04(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM04, val);
		return editor.commit();
	}

	public boolean setScore150Exam05(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM05, val);
		return editor.commit();
	}

	public boolean setScore150Exam06(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM06, val);
		return editor.commit();
	}

	public boolean setScore150Exam07(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM07, val);
		return editor.commit();
	}

	public boolean setScore150Exam08(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_150_EXAM08, val);
		return editor.commit();
	}

	//Get value
	public int getScore150Exam01() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM01, 0);
	}

	public int getScore150Exam02() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM02, 0);
	}

	public int getScore150Exam03() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM03, 0);
	}

	public int getScore150Exam04() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM04, 0);
	}

	public int getScore150Exam05() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM05, 0);
	}

	public int getScore150Exam06() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM06, 0);
	}

	public int getScore150Exam07() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM07, 0);
	}

	public int getScore150Exam08() {
		SharedPreferences pre = this.context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_150_EXAM08, 0);
	}

	/******************************************************************************/
	//HISTORY SCORE FOR EXAM 450

	//Get value
	public int getScore450Exam01() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM01, 0);
	}

	public int getScore450Exam02() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM02, 0);
	}

	public int getScore450Exam03() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM03, 0);
	}

	public int getScore450Exam04() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM04, 0);
	}

	public int getScore450Exam05() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM05, 0);
	}

	public int getScore450Exam06() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM06, 0);
	}

	public int getScore450Exam07() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM07, 0);
	}

	public int getScore450Exam08() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM08, 0);
	}

	public int getScore450Exam09() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM09, 0);
	}

	public int getScore450Exam10() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM10, 0);
	}

	public int getScore450Exam11() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM11, 0);
	}

	public int getScore450Exam12() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM12, 0);
	}

	public int getScore450Exam13() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM13, 0);
	}

	public int getScore450Exam14() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM14, 0);
	}

	public int getScore450Exam15() {
		SharedPreferences pre = context.getSharedPreferences(PRE_STUDY, MODE);
		return pre.getInt(SCORE_450_EXAM15, 0);
	}

	//Set value
	public boolean setScore450Exam01(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM01, val);
		return editor.commit();
	}

	public boolean setScore450Exam02(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM02, val);
		return editor.commit();
	}

	public boolean setScore450Exam03(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM03, val);
		return editor.commit();
	}

	public boolean setScore450Exam04(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM04, val);
		return editor.commit();
	}

	public boolean setScore450Exam05(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM05, val);
		return editor.commit();
	}

	public boolean setScore450Exam06(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM06, val);
		return editor.commit();
	}

	public boolean setScore450Exam07(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM07, val);
		return editor.commit();
	}
	public boolean setScore450Exam08(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM08, val);
		return editor.commit();
	}

	public boolean setScore450Exam09(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM09, val);
		return editor.commit();
	}

	public boolean setScore450Exam10(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM10, val);
		return editor.commit();
	}

	public boolean setScore450Exam11(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM11, val);
		return editor.commit();
	}

	public boolean setScore450Exam12(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM12, val);
		return editor.commit();
	}

	public boolean setScore450Exam13(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM13, val);
		return editor.commit();
	}

	public boolean setScore450Exam14(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM14, val);
		return editor.commit();
	}

	public boolean setScore450Exam15(int val) {
		Editor editor = getEditor();
		editor.putInt(SCORE_450_EXAM15, val);
		return editor.commit();
	}
}
