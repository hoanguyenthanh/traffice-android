package com.htn.traffic.handledata;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.htn.traffic.UIApplication;
import com.htn.traffic.handledata.xmlhandle.DBHelper;
import com.htn.traffic.models.Topic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TopicDAL {
	private SQLiteDatabase db;
	public TopicDAL(SQLiteDatabase db) {
		this.db = UIApplication.getConnection();
	}

	/**
	 * Insert Topic
	 * @param topic
	 * @return
	 * 	the row ID of the newly inserted row, or -1 if an error occurred
     */
	public long insert(Topic topic) {
		long out = db.insert(DBHelper.TOPIC_TABLE_NAME, null, this.getContentValues(topic));
		return out;
	}

	/**
	 * Update Topic
	 * @param topic
	 * @return the number of rows affected
     */
	public long update(Topic topic) {
		return db.update(DBHelper.TOPIC_TABLE_NAME
				, this.getContentValues(topic)
				, "_id = ?"
				, new String[] {topic.getId() + ""});
	}

	/**
	 * Delete Topic by Id
	 * @param id
	 * @return
	 * 	The number of rows affected if a whereClause is passed in, 0 otherwise.
	 * 	To remove all rows and get a count pass "1" as the whereClause.
     */
	public int delete(Integer id) {
		return db.delete(DBHelper.TOPIC_TABLE_NAME
				, "_id = ?"
				, new String[] { id.toString() });
	}

	/**
	 * Tra ve 1 Topics theo primary key trong DB
	 * 
	 * @param topicId
	 * @return Topic
	 */
	public Topic getTopicByID(int id) {
		Topic topic = new Topic();
		Cursor cursor = db.query(DBHelper.TOPIC_TABLE_NAME
				, DBHelper.TOPIC_COLUMNS
				, "_id = ?"
				, new String[] { "" + id }
				,null, null, null);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			topic = convertCursor2Topic(cursor);
		}

		if (cursor != null) {
			cursor.close();
		}
		return topic;
	}

	public Topic getTopicByID(int id, Collection<Topic> list) {
		for (Topic topic : list) {
			if (id == topic.getId()) {
				return topic;
			}
		}
		return null;
	}

	public List<Topic> getTopicsByCategory(String category) {
		List<Topic> list = new ArrayList<Topic>();
		Cursor cursor = db.query(DBHelper.TOPIC_TABLE_NAME
				, DBHelper.TOPIC_COLUMNS
				, DBHelper.TOPIC_CATEGORY + " = ?"
				, new String[] { "" + category }, null, null, null);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Topic(cursor));
		}

		if (cursor != null) {
			cursor.close();
		}
		return list;
	}

	public List<Topic> getTopics() {
		List<Topic> list = new ArrayList<Topic>();
		Cursor cursor = db.query(DBHelper.TOPIC_TABLE_NAME,
				DBHelper.TOPIC_COLUMNS, null, null, null, null, null);
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(convertCursor2Topic(cursor));
		}

		cursor.close();
		return list;
	}

	private Topic convertCursor2Topic(Cursor cursor) {
		Topic topic = new Topic();
		topic.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.TOPIC_ID)));
		topic.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.TOPIC_TITLE)));
		topic.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.TOPIC_DESCRIPTION)));
		topic.setCategory(cursor.getString(cursor.getColumnIndex(DBHelper.TOPIC_CATEGORY)));
		return topic;
	}

	public ContentValues getContentValues(Topic topic) {
		ContentValues values = new ContentValues();
		// id tự tăng nên ko can put(id)
		values.put(DBHelper.TOPIC_TITLE, topic.getTitle());
		values.put(DBHelper.TOPIC_CATEGORY, topic.getCategory());
		values.put(DBHelper.TOPIC_DESCRIPTION, topic.getDescription());
		return values;
	}

	/**
	 * Remove duplicates rows in Topics table
	 */
	public void removeDuplicates() {
		String sql = "delete from " + DBHelper.TOPIC_TABLE_NAME
				+ " where rowid in"
				+ " (select rowid from " + DBHelper.TOPIC_TABLE_NAME + " as Duplicates "
				+ " 	where rowid >"
				+ " ( select min(rowid) from " + DBHelper.TOPIC_TABLE_NAME
				+ " 	as First where " + "First." + DBHelper.TOPIC_TITLE
				+ " = Duplicates." + DBHelper.TOPIC_TITLE + "));";
		db.execSQL(sql);
		sql = null;
	}
}