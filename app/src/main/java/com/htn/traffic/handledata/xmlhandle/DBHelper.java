package com.htn.traffic.handledata.xmlhandle;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 11;
	private SQLiteDatabase db = null;
	Context context = null;
	private static final String DB_NAME = "traffic.db";
	private static final CursorFactory CURSOR_FACTORY = null;

	public static final String QUESTION_XML_TAG_NAME = "question";
	public static final String TOPIC_XML_TAG_NAME = "topic";
	public static final String CHOICE_XML_TAG_NAME = "choice";

	public static final String QUESTION_TABLE_NAME = "questions";
	public static final String QUESTION_ID = "_id";
	public static final String QUESTION_SORT_ORDER = "sortOrder";
	public static final String QUESTION_TITLE = "question_title";
	public static final String QUESTION_IMAGE = "question_image";
	public static final String QUESTION_IS_MULTI = "isMulti";
	public static final String QUESTION_IS_MARK = "isMark";
	public static final String QUESTION_TOPIC = "topic";
	public static final String QUESTION_E150 = "e150";
	public static final String QUESTION_E450 = "e450";

	// Hang so cho table TOPIC
	public static final String TOPIC_TABLE_NAME = "topics";
	public static final String TOPIC_ID = "_id";
	public static final String TOPIC_TITLE = "topic_title";
	public static final String TOPIC_CATEGORY = "category";
	public static final String TOPIC_DESCRIPTION = "description";

	// Hang so cho table CHOICE
	public static final String CHOICE_TABLE_NAME = "choices";
	public static final String CHOICE_ID = "_id";
	public static final String CHOICE_TITLE = "choice_title";
	public static final String CHOICE_IS_CORRECT = "isCorrect";
	public static final String CHOICE_QUESTION_ID = "question_id";

	public static final String[] QUESTION_COLUMNS = { QUESTION_ID
			, QUESTION_SORT_ORDER
			, QUESTION_TITLE
			, QUESTION_IMAGE
			, QUESTION_IS_MULTI
			, QUESTION_IS_MARK
			, QUESTION_TOPIC
			, QUESTION_E150
			, QUESTION_E450};
	public static final String[] TOPIC_COLUMNS = { TOPIC_ID, TOPIC_TITLE, TOPIC_CATEGORY, TOPIC_DESCRIPTION };
	public static final String[] CHOICE_COLUMNS = { CHOICE_ID, CHOICE_TITLE, CHOICE_IS_CORRECT, CHOICE_QUESTION_ID };

	// SQL CREATE QUESTION
	private static final String QUESTION_CREATE = "create table "
			+ QUESTION_TABLE_NAME + "(" + QUESTION_ID + " integer primary key autoincrement, "
			+ QUESTION_SORT_ORDER + " integer, "
			+ QUESTION_TITLE + " text not null, "
			+ QUESTION_IMAGE + " text, "
			+ QUESTION_IS_MULTI + " integer, "
			+ QUESTION_IS_MARK + " integer, "
			+ QUESTION_TOPIC + " integer, "
			+ QUESTION_E150 + " integer, "
			+ QUESTION_E450 + " integer, "
			+ " UNIQUE(" + QUESTION_TITLE + ", " + QUESTION_SORT_ORDER + ") ON CONFLICT IGNORE);";

	// SQL CREATE TOPIC
	private static final String TOPIC_CREATE = "create table "
			+ TOPIC_TABLE_NAME + "(" + TOPIC_ID + " integer primary key autoincrement, "
			+ TOPIC_TITLE + " text not null, "
			+ TOPIC_CATEGORY + " text, "
			+ TOPIC_DESCRIPTION + " text, UNIQUE(" + TOPIC_TITLE + ") ON CONFLICT IGNORE);";

	// SQL CREATE CHOICE
	private static final String CHOICE_CREATE = "create table "
			+ CHOICE_TABLE_NAME + "(" + CHOICE_ID + " integer primary key autoincrement, "
			+ CHOICE_TITLE + " text not null, "
			+ CHOICE_IS_CORRECT + " integer, "
			+ CHOICE_QUESTION_ID + " integer);";

	// statement delete tables if exists
	private static final String QUESTION_DROP = "DROP TABLE IF EXISTS " + QUESTION_TABLE_NAME + ";";
	private static final String TOPIC_DROP = "DROP TABLE IF EXISTS " + TOPIC_TABLE_NAME + ";";
	private static final String CHOICE_DROP = "DROP TABLE IF EXISTS " + CHOICE_TABLE_NAME + ";";

	public DBHelper(Context ctx) {
		super(ctx, DB_NAME, CURSOR_FACTORY, DB_VERSION);
		this.context = ctx;
	}

	public void open() {
		if (isOpened() == false) {
			db = this.getWritableDatabase();
		}
	}

	public SQLiteDatabase getConnection()
	{
		return db;
	}

	@Override
	public synchronized void close() {
		if (isOpened())
			db.close();
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		db.close();
	}

	public boolean isOpened() {
		if (db != null)
			return true;
		return false;
	}

	/**
	 * Create tables
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.e("DBHelper:", "OnCreate() ");
		db.beginTransaction();

		try {
			db.execSQL(QUESTION_CREATE);
			db.execSQL(TOPIC_CREATE);
			db.execSQL(CHOICE_CREATE);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	/*
	 * Delete and create again table
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.beginTransaction();
		try {
			db.execSQL(QUESTION_DROP);
			db.execSQL(TOPIC_DROP);
			db.execSQL(CHOICE_DROP);

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		onCreate(db);
	}

}
