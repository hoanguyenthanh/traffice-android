package com.htn.traffic.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.htn.traffic.R;
import com.htn.traffic.helpers.Constants;

/**
 * Created date: 17/12/2016
 * Class name: Menu study motorbike
 */
public class MotorBikeLowActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.low_motor_layout);

        // All 150 question
        Button btnAllTopic = findViewById(R.id.mmsd01_btnAllTopic);

        //Khai niem va quy tac giao thong
        Button btnTopic1 = findViewById(R.id.mmsd01_btnTopic01);

        //Van hoa, dao duc nguoi lai xe
        Button btnTopic3 = findViewById(R.id.mmsd01_btnTopic03);

        //He thong bien bao
        Button btnTopic6 = findViewById(R.id.mmsd01_btnTopic06);

        //Cac the sa hinh
        Button btnTopic7 = findViewById(R.id.mmsd01_btnTopic07);
        Button btnToReview = findViewById(R.id.mmsd01_btnToReview);
        Button btnBackHome = findViewById(R.id.mmsd01_btnBackHome);

        btnAllTopic.setOnClickListener(this);
        btnTopic1.setOnClickListener(this);
        btnTopic3.setOnClickListener(this);
        btnTopic6.setOnClickListener(this);
        btnTopic7.setOnClickListener(this);
        btnToReview.setOnClickListener(this);
        btnBackHome.setOnClickListener(this);
    }

    private void startStudy(int topicId){
        Intent intent = new Intent(this, StudyActivity.class);
        intent.putExtra(Constants.SCREEN_ID, Constants.MOTORBIKE_LOW_ACTIVITY);
        intent.putExtra(Constants.TOPIC_ID, topicId);

        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mmsd01_btnAllTopic:
                startStudy(Constants.TOPIC_150);
                break;
            case R.id.mmsd01_btnTopic01:
                startStudy(Constants.TOPIC_01);
                break;
            case R.id.mmsd01_btnTopic03:
                startStudy(Constants.TOPIC_03);
                break;
            case R.id.mmsd01_btnTopic06:
                startStudy(Constants.TOPIC_06);
                break;
            case R.id.mmsd01_btnTopic07:
                startStudy(Constants.TOPIC_07);
                break;
            case R.id.mmsd01_btnToReview:
                startStudy(Constants.TOPIC_REVIEW_150);
                break;
            case R.id.mmsd01_btnBackHome:
                MotorBikeLowActivity.this.finish();
                break;
        }
    }
}
