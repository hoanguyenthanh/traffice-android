package com.htn.traffic.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;


import com.htn.traffic.R;
import com.htn.traffic.adapters.SignAdapter;
import com.htn.traffic.helpers.Constants;

/**
 * Created date: 13/12/2016
 * Class name: Traffic Sign
 */
public class SignActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private ListView mListView = null;
    private String[] mListItem = {"BIỂN BÁO CẤM"
            , "BIỂN BÁO NGUY HIỂM"
            , "BIỂN HIỆU LỆNH"
            , "BIỂN CHỈ DẪN"
            , "BIỂN PHỤ"
            , "VẠCH KẺ ĐƯỜNG"};

    private SignAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_layout);

        mListView = findViewById(R.id.ts01_list_view);
        findViewById(R.id.ts01_btnBackHome).setOnClickListener(this);

        adapter = new SignAdapter(getApplicationContext(), mListItem);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ts01_btnBackHome:
            finish();
            break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getApplicationContext(), SignDetailActivity.class);
        int iTopicID = 0;
        switch (position){
            case Constants.ITEM_BB_CAM:
                iTopicID = Constants.ITEM_BB_CAM;
                break;
            case Constants.ITEM_BB_NGUY_HIEM:
                iTopicID = Constants.ITEM_BB_NGUY_HIEM;
                break;
            case Constants.ITEM_BB_HIEU_LENH:
                iTopicID = Constants.ITEM_BB_HIEU_LENH;
                break;
            case Constants.ITEM_BB_CHI_DAN:
                iTopicID = Constants.ITEM_BB_CHI_DAN;
                break;
            case Constants.ITEM_BB_PHU:
                iTopicID = Constants.ITEM_BB_PHU;
                break;
            case Constants.ITEM_BB_VACH_KE_DUONG:
                iTopicID = Constants.ITEM_BB_VACH_KE_DUONG;
                break;
        }
        if (intent != null) {
            intent.putExtra(Constants.TOPIC_NAME, iTopicID);
            startActivity(intent);
        }
    }
}
