package com.htn.traffic.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.htn.traffic.R;
import com.htn.traffic.handledata.HistoryStudyDAL;
import com.htn.traffic.helpers.Constants;
import com.htn.traffic.helpers.ExamCarFactory;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;

import static com.htn.traffic.helpers.Constants.KEY_LEVEL;
import static com.htn.traffic.helpers.Constants.KEY_QUESTS;
import static com.htn.traffic.helpers.Constants.KEY_TITLE;

/**
 * Created date: 17/12/2016
 * Class name: menu exam of Car
 */
public class CarExamActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int correctPass = 28;

    private List<Question> mListQuestion = new ArrayList<>();
    private HistoryStudyDAL mHistoryStudyDAL;

    private Button btnExam1, btnExam2, btnExam3, btnExam4, btnExam5, btnExam6, btnExam7, btnExam8;
    private Button btnExam9, btnExam10, btnExam11, btnExam12, btnExam13, btnExam14, btnExam15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_car_layout);

        if (getActionBar() != null){
            getActionBar().hide();
        }

        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setScoreToButton();
    }

    private void initialize(){
        mHistoryStudyDAL = new HistoryStudyDAL(getApplicationContext());

        Button btnBackHome = findViewById(R.id.mmex0102_btnBackHome);
        Button btnRandom = findViewById(R.id.mmex0102_btnRandom);
        btnExam1 = findViewById(R.id.mmex0102_btnExam1);
        btnExam2 = findViewById(R.id.mmex0102_btnExam2);
        btnExam3 = findViewById(R.id.mmex0102_btnExam3);
        btnExam4 = findViewById(R.id.mmex0102_btnExam4);
        btnExam5 = findViewById(R.id.mmex0102_btnExam5);
        btnExam6 = findViewById(R.id.mmex0102_btnExam6);
        btnExam7 = findViewById(R.id.mmex0102_btnExam7);
        btnExam8 = findViewById(R.id.mmex0102_btnExam8);
        btnExam9 = findViewById(R.id.mmex0102_btnExam9);
        btnExam10 = findViewById(R.id.mmex0102_btnExam10);
        btnExam11 = findViewById(R.id.mmex0102_btnExam11);
        btnExam12 = findViewById(R.id.mmex0102_btnExam12);
        btnExam13 = findViewById(R.id.mmex0102_btnExam13);
        btnExam14 = findViewById(R.id.mmex0102_btnExam14);
        btnExam15 = findViewById(R.id.mmex0102_btnExam15);

        btnBackHome.setOnClickListener(this);
        btnRandom.setOnClickListener(this);
        btnExam1.setOnClickListener(this);
        btnExam2.setOnClickListener(this);
        btnExam3.setOnClickListener(this);
        btnExam4.setOnClickListener(this);
        btnExam5.setOnClickListener(this);
        btnExam6.setOnClickListener(this);
        btnExam7.setOnClickListener(this);
        btnExam8.setOnClickListener(this);
        btnExam9.setOnClickListener(this);
        btnExam10.setOnClickListener(this);
        btnExam11.setOnClickListener(this);
        btnExam12.setOnClickListener(this);
        btnExam13.setOnClickListener(this);
        btnExam14.setOnClickListener(this);
        btnExam15.setOnClickListener(this);
    }

    public void setScoreToButton(){
        int iScore = mHistoryStudyDAL.getScore450Exam01();
        if (iScore >= correctPass){
            btnExam1.setText(getResultTextPass(1, iScore));
        }else if (iScore > 0){
            btnExam1.setText(getResultTextNotPass(1, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam02();
        if (iScore >= correctPass){
            btnExam2.setText(getResultTextPass(2, iScore));
        }else if (iScore > 0){
            btnExam2.setText(getResultTextNotPass(2, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam03();
        if (iScore >= correctPass){
            btnExam3.setText(getResultTextPass(3, iScore));
        }else if (iScore > 0){
            btnExam3.setText(getResultTextNotPass(3, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam04();
        if (iScore >= correctPass){
            btnExam4.setText(getResultTextPass(4, iScore));
        }else if (iScore > 0){
            btnExam4.setText(getResultTextNotPass(4, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam05();
        if (iScore >= correctPass){
            btnExam5.setText(getResultTextPass(5, iScore));
        }else if (iScore > 0){
            btnExam5.setText(getResultTextNotPass(5, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam06();
        if (iScore >= correctPass){
            btnExam6.setText(getResultTextPass(6, iScore));
        }else if (iScore > 0){
            btnExam6.setText(getResultTextNotPass(6, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam07();
        if (iScore >= correctPass){
            btnExam7.setText(getResultTextPass(7, iScore));
        }else if (iScore > 0){
            btnExam7.setText(getResultTextNotPass(7, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam08();
        if (iScore >= correctPass){
            btnExam8.setText(getResultTextPass(8, iScore));
        }else if (iScore > 0){
            btnExam8.setText(getResultTextNotPass(8, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam09();
        if (iScore >= correctPass){
            btnExam9.setText(getResultTextPass(9, iScore));
        }else if (iScore > 0){
            btnExam9.setText(getResultTextNotPass(9, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam10();
        if (iScore >= correctPass){
            btnExam10.setText(getResultTextPass(10, iScore));
        }else if (iScore > 0){
            btnExam10.setText(getResultTextNotPass(10, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam11();
        if (iScore >= correctPass){
            btnExam11.setText(getResultTextPass(11, iScore));
        }else if (iScore > 0){
            btnExam11.setText(getResultTextNotPass(11, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam12();
        if (iScore >= correctPass){
            btnExam12.setText(getResultTextPass(12, iScore));
        }else if (iScore > 0){
            btnExam12.setText(getResultTextNotPass(12, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam13();
        if (iScore >= correctPass){
            btnExam13.setText(getResultTextPass(13, iScore));
        }else if (iScore > 0){
            btnExam13.setText(getResultTextNotPass(13, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam14();
        if (iScore >= correctPass){
            btnExam14.setText(getResultTextPass(14, iScore));
        }else if (iScore > 0){
            btnExam14.setText(getResultTextNotPass(14, iScore));
        }

        iScore = mHistoryStudyDAL.getScore450Exam15();
        if (iScore >= correctPass){
            btnExam15.setText(getResultTextPass(15, iScore));
        }else if (iScore > 0){
            btnExam15.setText(getResultTextNotPass(15, iScore));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mmex0102_btnRandom:
                startExamDetail(Constants.TOPIC_450, true);
                break;
            case R.id.mmex0102_btnExam1:
                startExamDetail(1, false);
                break;
            case R.id.mmex0102_btnExam2:
                startExamDetail(2, false);
                break;
            case R.id.mmex0102_btnExam3:
                startExamDetail(3, false);
                break;
            case R.id.mmex0102_btnExam4:
                startExamDetail(4, false);
                break;
            case R.id.mmex0102_btnExam5:
                startExamDetail(5, false);
                break;
            case R.id.mmex0102_btnExam6:
                startExamDetail(6, false);
                break;
            case R.id.mmex0102_btnExam7:
                startExamDetail(7, false);
                break;
            case R.id.mmex0102_btnExam8:
                startExamDetail(8, false);
                break;
            case R.id.mmex0102_btnExam9:
                startExamDetail(9, false);
                break;
            case R.id.mmex0102_btnExam10:
                startExamDetail(10, false);
                break;
            case R.id.mmex0102_btnExam11:
                startExamDetail(11, false);
                break;
            case R.id.mmex0102_btnExam12:
                startExamDetail(12, false);
                break;
            case R.id.mmex0102_btnExam13:
                startExamDetail(13, false);
                break;
            case R.id.mmex0102_btnExam14:
                startExamDetail(14, false);
                break;
            case R.id.mmex0102_btnExam15:
                startExamDetail(15, false);
                break;
            case R.id.mmex0102_btnBackHome:
                finish();
                break;
        }
    }

    private void startExamDetail(int examId, boolean random){
        ExamCarFactory examCarFactory = new ExamCarFactory();
        examCarFactory.factoryExam(examId);

        mListQuestion = examCarFactory.getListQuest();
        String title = random ? "Bộ đề ngẫu nhiên" : "Bộ đề " + examId;

        Intent intent = new Intent(this, ExamActivity.class);
        intent.putParcelableArrayListExtra(KEY_QUESTS, (ArrayList<? extends Parcelable>) mListQuestion);
        intent.putExtra(KEY_TITLE, title);
        intent.putExtra(KEY_LEVEL, "car");

        startActivity(intent);
    }

    private String getResultTextPass(int examId, int score){
        return "Bộ đề " + examId + " (Hoàn thành " + score + "/30: Đạt)";
    }

    private String getResultTextNotPass(int examId, int score){
        return "Bộ đề " + examId + " (Hoàn thành " + score + "/30: Chưa đạt)";
    }
}
