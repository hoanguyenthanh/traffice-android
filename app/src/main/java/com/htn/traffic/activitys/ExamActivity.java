package com.htn.traffic.activitys;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.adapters.QuestionExamAdapter;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.htn.traffic.helpers.Constants.KEY_EXAM_ID;
import static com.htn.traffic.helpers.Constants.KEY_LEVEL;
import static com.htn.traffic.helpers.Constants.KEY_QUESTS;
import static com.htn.traffic.helpers.Constants.KEY_TITLE;

/**
 * Created date: 14/12/2016
 * By: HoaNguyen
 * Class name: Exam
 */
public class ExamActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener{
	public static final String TAG = "ExamActivity";

	private ViewPager viewPager;
	private Button btnApply;
	private Button btnBack;
	private TextView txtTimer;
	private TextView txtTitleExam;

	private List<Question> mListQuestion;
	private QuestionExamAdapter mQuestionAdapter;
	private int mCurPosition;

	private String level = "";
	private int examId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exam_detail_layout);

		if (getSupportActionBar() != null) {
			getSupportActionBar().hide();
		}

		viewPager = findViewById(R.id.ex02_viewPager);
		btnApply = findViewById(R.id.ex02_btnApply);
		btnBack = findViewById(R.id.ex02_btnBack);
		txtTimer = findViewById(R.id.ex02_txtTimer);
		txtTitleExam = findViewById(R.id.ex02_txtTitleExam);

		btnApply.setOnClickListener(this);
		btnBack.setOnClickListener(this);

		Bundle bundle = getIntent().getExtras();
		String title = "";

		if (bundle != null){
			level = bundle.getString(KEY_LEVEL);
			examId = bundle.getInt(KEY_EXAM_ID);
			mListQuestion = bundle.getParcelableArrayList(KEY_QUESTS);
			title = bundle.getString(KEY_TITLE);
		}

		txtTitleExam.setText(title);
		resetChoices();

		mQuestionAdapter = new QuestionExamAdapter(this, mListQuestion, this);
		viewPager.setAdapter(mQuestionAdapter);

		setStartTimer();
	}

	@Override
	protected void onResume() {
		super.onResume();
		viewPager.addOnPageChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		viewPager.removeOnPageChangeListener(this);
	}


	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.ex02_btnBack:
				finish();
				break;
			case R.id.ex02_btnApply:
				finishExam();
				break;
			case R.id.ex0201_btnAns1:
				setAnswer(0, v);
				break;
			case R.id.ex0201_btnAns2:
				setAnswer(1, v);
				break;
			case R.id.ex0201_btnAns3:
				setAnswer(2, v);
				break;
			case R.id.ex0201_btnAns4:
				setAnswer(3, v);
				break;
		}
	}


	private void setAnswer(int numAns, View button){
		int valAns = mListQuestion.get(mCurPosition).getValueAnswer(numAns);
		int answer = valAns == 0 ? 1 : 0;
		mListQuestion.get(mCurPosition).setValueAnswer(numAns, answer);

		Drawable tick = getResources().getDrawable(R.drawable.ic_tick);
		Drawable unTick = getResources().getDrawable(R.drawable.ic_untick);
		button.setBackground(answer == 1 ?  tick : unTick);
	}

	private void resetChoices(){
		if (mListQuestion != null) {
			for (Question question : mListQuestion) {
				question.setListChoice();
				question.resetValueAnswer();
			}
		}
	}

	private void pageNext(){
		if (mCurPosition < mListQuestion.size() - 1){
			mCurPosition++;
		}
		viewPager.setCurrentItem(mCurPosition);
	}

	private void pagePrev(){
		if (mCurPosition > 0){
			mCurPosition--;
		}
		viewPager.setCurrentItem(mCurPosition);
	}

	private void finishExam(){
		Intent intent = new Intent(getApplicationContext(), ResultExamActivity.class);

		intent.putExtra(KEY_LEVEL, level);
		intent.putExtra(KEY_EXAM_ID, examId);
		intent.putParcelableArrayListExtra(KEY_QUESTS, (ArrayList<? extends Parcelable>) this.mListQuestion);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
		configExit();
	}

	private void configExit() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setMessage("Bạn có chắc muốn thoát?");
		alert.setPositiveButton("Thoát", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ExamActivity.this.finish();
			}
		});
		alert.setNegativeButton("Đóng",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (dialog != null) {
							dialog.dismiss();
						}
					}
				});

		alert.show();
	}

	private void setStartTimer(){
		long future = "motor".equals(level) ?  1000 * 60 * 20 : 1000 * 60 * 30;
		MyCountDownTimer countDownTimer = new MyCountDownTimer(future, 1000);

		if (countDownTimer != null) {
			countDownTimer.cancel();
			countDownTimer.start();
		}
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		mCurPosition = position;
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}


	private class MyCountDownTimer extends CountDownTimer {
		StringBuilder stringBuilder = new StringBuilder();
		long minute;

		public MyCountDownTimer(long future, long interval){
			super(future, interval);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			minute = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;
			stringBuilder.setLength(0);
			stringBuilder.append(String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
			stringBuilder.append(":");
			stringBuilder.append(String.format("%02d", minute));
			txtTimer.setText(stringBuilder.toString());
		}

		@Override
		public void onFinish() {
			finishExam();
		}
	}
}
