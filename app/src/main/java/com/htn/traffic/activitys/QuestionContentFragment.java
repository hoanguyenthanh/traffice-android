package com.htn.traffic.activitys;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.UIApplication;
import com.htn.traffic.helpers.SFSetting;
import com.htn.traffic.models.Choice;
import com.htn.traffic.models.Question;

import java.util.List;

public class QuestionContentFragment extends Fragment {
    public static final String TAG = "QuestionContentFragment";

    private SFSetting sfSetting;
    private Question mQuestion;
    private int numberDisplay;
    private Context context;

    private TextView txtQNumber, txtTitle, txtAnswer;
    private TextView txtQ1, txtQ2, txtQ3, txtQ4;
    private ImageView imageView;

    public QuestionContentFragment() {
    }

    public static QuestionContentFragment newInstance(Question _question, int questionNumber) {
        Bundle args = new Bundle();
        args.putParcelable("question", _question);
        args.putInt("numberDisplay", questionNumber);

        QuestionContentFragment fragment = new QuestionContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        sfSetting = new SFSetting(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.question_study_holder, container, false);

        txtQNumber = view.findViewById(R.id.sd0101_txtQNum);
        txtTitle = view.findViewById(R.id.sd0101_txtTitle);
        imageView = view.findViewById(R.id.sd0101_imgView);
        txtAnswer = view.findViewById(R.id.sd0101_txtAnswer);

        txtQ1 = view.findViewById(R.id.sd0101_txtQuest1);
        txtQ2 = view.findViewById(R.id.sd0101_txtQuest2);
        txtQ3 = view.findViewById(R.id.sd0101_txtQuest3);
        txtQ4 = view.findViewById(R.id.sd0101_txtQuest4);

        txtQ3.setVisibility(View.GONE);
        txtQ4.setVisibility(View.GONE);

        txtQNumber.setText("Câu " + (numberDisplay + 1) + ":");
        txtTitle.setText(mQuestion.title);
        txtAnswer.setTag("txtAnswer" + numberDisplay);



        Drawable drawable = getDrawable(mQuestion.image);
        if(drawable != null){
            imageView.setImageDrawable(drawable);
        }else{
            imageView.setVisibility(View.GONE);
        }

        boolean showAnswer = sfSetting.getShowAnswer();
        this.showAnswer(showAnswer, mQuestion);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            mQuestion = getArguments().getParcelable("question");
            numberDisplay = getArguments().getInt("numberDisplay");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public Drawable getDrawable(String name) {
        Drawable drawable = null;
        int sourceId = getActivity().getResources().getIdentifier(name, "drawable", getActivity().getPackageName());
        if (sourceId > 0){
            try {
                drawable = getActivity().getResources().getDrawable(sourceId);
            }catch (Resources.NotFoundException e){
                drawable = null;
            }
        }
        return drawable;
    }

    public void setBlackText(){
        if (context == null){
            return;
        }
        int black = context.getResources().getColor(R.color.black);

        if (txtQ1 != null){
            txtQ1.setTextColor(black);
        }

        if (txtQ2 != null){
            txtQ2.setTextColor(black);
        }

        if (txtQ3 != null){
            txtQ3.setTextColor(black);
        }

        if (txtQ4 != null){
            txtQ4.setTextColor(black);
        }
    }

    public void showAnswer(boolean showAnswer, Question question){
        if (context == null || question == null || txtQ1 == null || txtQ2 == null || txtQ3 == null || txtQ4 == null){
            return;
        }

        int index = 0;
        List<Choice> choiceList = UIApplication.getChoiceDAL().getChoices(question.questId);
        final int black = context.getResources().getColor(R.color.black);
        final int blue = context.getResources().getColor(R.color.blue);

        txtQ1.setTextColor(black);
        txtQ2.setTextColor(black);
        txtQ3.setTextColor(black);
        txtQ4.setTextColor(black);

        for (Choice choice : choiceList) {
            switch (index){
                case 0:
                    txtQ1.setText(Html.fromHtml(choice.getTitle().trim()));

                    if (choice.isCorrect() && showAnswer) {
                        txtQ1.setTextColor(blue);
                    }
                    break;
                case 1:
                    txtQ2.setText(Html.fromHtml(choice.getTitle().trim()));
                    if (choice.isCorrect() && showAnswer) {
                        txtQ2.setTextColor(blue);
                    }
                    break;
                case 2:
                    txtQ3.setVisibility(View.VISIBLE);
                    txtQ3.setText(Html.fromHtml(choice.getTitle().trim()));

                    if (choice.isCorrect() && showAnswer) {
                        txtQ3.setTextColor(blue);
                    }
                    break;
                case 3:
                    txtQ4.setVisibility(View.VISIBLE);
                    txtQ4.setText(Html.fromHtml(choice.getTitle().trim()));

                    if (choice.isCorrect() && showAnswer) {
                        txtQ4.setTextColor(blue);
                    }
                    break;
            }
            index++;
        }
    }
}
