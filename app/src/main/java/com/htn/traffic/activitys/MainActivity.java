package com.htn.traffic.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.htn.traffic.R;
import com.htn.traffic.adapters.MenuAdapter;
import com.htn.traffic.models.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String ITEM_MOTOR_LOW = "motor.low";
    public static final String ITEM_MOTOR_EXAM = "motor.exam";
    public static final String ITEM_CAR_LOW = "car.low";
    public static final String ITEM_CAR_EXAM = "car.exam";
    public static final String ITEM_SIGNS = "signs";
    public static final String ITEM_INTRODUCTION = "introduction";


    private List<MenuItem> menuItems = new ArrayList<>();
    private RecyclerView rvRecyclerView;
    private MenuAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        rvRecyclerView = findViewById(R.id.rvRecyclerView);

        menuItems.add(new MenuItem(ITEM_MOTOR_LOW, getString(R.string.str_menu_motor_low)));
        menuItems.add(new MenuItem(ITEM_CAR_LOW, getString(R.string.str_menu_car_low)));
        menuItems.add(new MenuItem(ITEM_MOTOR_EXAM, getString(R.string.str_menu_motor_exam)));
        menuItems.add(new MenuItem(ITEM_CAR_EXAM, getString(R.string.str_menu_car_exam)));
        menuItems.add(new MenuItem(ITEM_SIGNS, getString(R.string.str_menu_signs)));
        menuItems.add(new MenuItem(ITEM_INTRODUCTION, getString(R.string.str_menu_introduction)));

        adapter = new MenuAdapter(this.menuItems, new MenuAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MenuItem item) {
                startActivity(item);
            }
        });

        rvRecyclerView.setAdapter(adapter);
        rvRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvRecyclerView.setLayoutManager(mLayoutManager);


    }


    private void startActivity(MenuItem items){
        final String key = items.key;
        Intent intent;
        switch (key){
            case ITEM_MOTOR_LOW:
                intent = new Intent(this, MotorBikeLowActivity.class);
                startActivity(intent);
                break;

            case ITEM_MOTOR_EXAM:
                intent = new Intent(this, MotorbikeExamActivity.class);
                startActivity(intent);
                break;

            case ITEM_CAR_LOW:
                intent = new Intent(this, CarLowActivity.class);
                startActivity(intent);
                break;

            case ITEM_CAR_EXAM:
                intent = new Intent(this, CarExamActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case ITEM_SIGNS:
                intent = new Intent(this, SignActivity.class);
                startActivity(intent);
                break;

            case ITEM_INTRODUCTION:

                break;
        }
    }
}
