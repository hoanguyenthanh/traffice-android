package com.htn.traffic.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.adapters.ResultExamAdapter;
import com.htn.traffic.handledata.HistoryStudyDAL;
import com.htn.traffic.helpers.Constants;
import com.htn.traffic.models.Choice;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;

import static com.htn.traffic.helpers.Constants.KEY_EXAM_ID;
import static com.htn.traffic.helpers.Constants.KEY_LEVEL;
import static com.htn.traffic.helpers.Constants.KEY_QUESTS;
import static com.htn.traffic.helpers.Constants.LEVEL_CAR;



/**
 * By: Hoa Nguyen
 */
public class ResultExamActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView mListView;
    private TextView txtResult, txtDescription;
    private Button btnAgain, btnExit;
    private ResultExamAdapter mResultAdapter;

    private int examId;
    private String level = "";

    private HistoryStudyDAL historyStudyDAL;
    private List<Question> questionList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_result_layout);

        if (getActionBar() != null){
            getActionBar().hide();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            level = bundle.getString(KEY_LEVEL);
            examId = bundle.getInt(KEY_EXAM_ID);
            questionList = bundle.getParcelableArrayList(KEY_QUESTS);
        }

        historyStudyDAL = new HistoryStudyDAL(getApplication());

        mListView = findViewById(R.id.ex03_listView);
        btnAgain = findViewById(R.id.ex03_btnAgain);
        btnExit = findViewById(R.id.ex03_btnExit);
        txtResult = findViewById(R.id.ex03_txtResult);
        txtDescription = findViewById(R.id.ex03_txtResultDescrip);

        btnAgain.setOnClickListener(this);
        btnExit.setOnClickListener(this);

        int score = calcScore();
        setScoreHistory(score);

        mResultAdapter = new ResultExamAdapter(getApplicationContext(), questionList);
        mListView.setAdapter(mResultAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ex03_btnAgain:
                finish();
                break;
            case R.id.ex03_btnExit:
                finish();
                break;
        }
    }

    private int calcScore(){
        int countCorrect = LEVEL_CAR.equals(level) ? 30 : 20;
        boolean isCorrect;

        for (Question question : questionList) {
            isCorrect = true;
            List<Choice> choices = question.getListChoice();

            int[] arrAns = question.answer;
            int countAns = arrAns[0] + arrAns[1] + arrAns[2] + arrAns[3] + arrAns[4];

            if (countAns > 0){
                for (int i = 0; i < choices.size(); i++) {
                    if (choices.get(i).getIsCorrect() != arrAns[i]) {
                        isCorrect = false;
                        countCorrect--;
                        break;
                    }
                }
            }else{
                isCorrect = false;
                countCorrect--;
            }

            question.setTrueFalse(isCorrect);
        }

        //Set text
        int rulePass = "car".equals(level) ? Constants.SCORE_PAST_CAR : Constants.SCORE_PAST_MOTOR;

        if (countCorrect >= rulePass){
            txtResult.setText("Kết quả: Đạt");
            txtDescription.setText("Đúng " + countCorrect + "/" + rulePass + " câu");
        }else{
            txtResult.setText("Kết quả: Không đạt");
            txtDescription.setText("Đúng " + countCorrect + " câu, phải đúng tối thiểu " + rulePass + " câu để đạt");
        }

        return countCorrect;
    }

    private void setScoreHistory(int countCorrect){
        if (Constants.LEVEL_MOTOR.equals(level)){
            switch (examId){
                case 1:
                    historyStudyDAL.setScore150Exam01(countCorrect);
                    break;
                case 2:
                    historyStudyDAL.setScore150Exam02(countCorrect);
                    break;
                case 3:
                    historyStudyDAL.setScore150Exam03(countCorrect);
                    break;
                case 4:
                    historyStudyDAL.setScore150Exam04(countCorrect);
                    break;
                case 5:
                    historyStudyDAL.setScore150Exam05(countCorrect);
                    break;
                case 6:
                    historyStudyDAL.setScore150Exam06(countCorrect);
                    break;
                case 7:
                    historyStudyDAL.setScore150Exam07(countCorrect);
                    break;
                case 8:
                    historyStudyDAL.setScore150Exam08(countCorrect);
                    break;
            }
        }else if (Constants.LEVEL_CAR.equals(level)){
            switch (examId){
                case 1:
                    historyStudyDAL.setScore450Exam01(countCorrect);
                    break;
                case 2:
                    historyStudyDAL.setScore450Exam02(countCorrect);
                    break;
                case 3:
                    historyStudyDAL.setScore450Exam03(countCorrect);
                    break;
                case 4:
                    historyStudyDAL.setScore450Exam04(countCorrect);
                    break;
                case 5:
                    historyStudyDAL.setScore450Exam05(countCorrect);
                    break;
                case 6:
                    historyStudyDAL.setScore450Exam06(countCorrect);
                    break;
                case 7:
                    historyStudyDAL.setScore450Exam07(countCorrect);
                    break;
                case 8:
                    historyStudyDAL.setScore450Exam08(countCorrect);
                    break;
                case 9:
                    historyStudyDAL.setScore450Exam09(countCorrect);
                    break;
                case 10:
                    historyStudyDAL.setScore450Exam10(countCorrect);
                    break;
                case 11:
                    historyStudyDAL.setScore450Exam11(countCorrect);
                    break;
                case 12:
                    historyStudyDAL.setScore450Exam12(countCorrect);
                    break;
                case 13:
                    historyStudyDAL.setScore450Exam13(countCorrect);
                    break;
                case 14:
                    historyStudyDAL.setScore450Exam14(countCorrect);
                    break;
                case 15:
                    historyStudyDAL.setScore450Exam15(countCorrect);
                    break;
            }
        }
    }
}

