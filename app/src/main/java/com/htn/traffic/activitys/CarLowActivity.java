package com.htn.traffic.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import com.htn.traffic.R;
import com.htn.traffic.helpers.Constants;

/**
 * Created date: 17/12/2016
 * Class name: Menu study Car
 */
public class CarLowActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.low_car_layout);
        initialize();
    }

    private void initialize(){
        Button btnAllTopic = findViewById(R.id.mmsd02_btnAllTopic);
        Button btnTopic1 = findViewById(R.id.mmsd02_btnTopic01);
        Button btnTopic2 = findViewById(R.id.mmsd02_btnTopic02);
        Button btnTopic3 = findViewById(R.id.mmsd02_btnTopic03);
        Button btnTopic4 = findViewById(R.id.mmsd02_btnTopic04);
        Button btnTopic5 = findViewById(R.id.mmsd02_btnTopic05);
        Button btnTopic6 = findViewById(R.id.mmsd02_btnTopic06);
        Button btnTopic7 = findViewById(R.id.mmsd02_btnTopic07);
        Button btnToReview = findViewById(R.id.mmsd02_btnToReview);
        Button btnBackHome = findViewById(R.id.mmsd02_btnBackHome);

        btnAllTopic.setOnClickListener(clickListener);
        btnTopic1.setOnClickListener(clickListener);
        btnTopic2.setOnClickListener(clickListener);
        btnTopic3.setOnClickListener(clickListener);
        btnTopic4.setOnClickListener(clickListener);
        btnTopic5.setOnClickListener(clickListener);
        btnTopic6.setOnClickListener(clickListener);
        btnTopic7.setOnClickListener(clickListener);
        btnToReview.setOnClickListener(clickListener);
        btnBackHome.setOnClickListener(clickListener);
    }

    private void startActivityTopic(int topicId){
        Intent intent = new Intent(getApplicationContext(), StudyActivity.class);
        intent.putExtra(Constants.SCREEN_ID, Constants.CAR_LOW_ACTIVITY);
        intent.putExtra(Constants.TOPIC_ID, topicId);
        startActivity(intent);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R.id.mmsd02_btnAllTopic:
                    startActivityTopic(450);
                    break;
                case R.id.mmsd02_btnTopic01:
                    startActivityTopic(Constants.TOPIC_01);
                    break;
                case R.id.mmsd02_btnTopic02:
                    startActivityTopic(Constants.TOPIC_02);
                    break;
                case R.id.mmsd02_btnTopic03:
                    startActivityTopic(Constants.TOPIC_03);
                    break;
                case R.id.mmsd02_btnTopic04:
                    startActivityTopic(Constants.TOPIC_04);
                    break;
                case R.id.mmsd02_btnTopic05:
                    startActivityTopic(Constants.TOPIC_05);
                    break;
                case R.id.mmsd02_btnTopic06:
                    startActivityTopic(Constants.TOPIC_06);
                    break;
                case R.id.mmsd02_btnTopic07:
                    startActivityTopic(Constants.TOPIC_07);
                    break;
                case R.id.mmsd02_btnToReview:
                    startActivityTopic(Constants.TOPIC_REVIEW_450);
                    break;
                case R.id.mmsd02_btnBackHome:
                    CarLowActivity.this.finish();
                    break;
            }
        }
    };
}
