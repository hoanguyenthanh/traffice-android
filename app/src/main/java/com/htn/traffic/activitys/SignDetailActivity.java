package com.htn.traffic.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ZoomControls;


import com.htn.traffic.R;
import com.htn.traffic.helpers.Constants;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created date: 13/12/2016
 */
public class SignDetailActivity extends AppCompatActivity implements OnClickListener{
	private ImageView imageView;
	private ZoomControls zoomControls;
	private Button btnExit;
	private TextView txtTitleBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_detail_layout);

		btnExit = findViewById(R.id.ts02_btnExit);
		txtTitleBar = findViewById(R.id.ts02_txtTitle);
		zoomControls = findViewById(R.id.ts02_zoomControl);
		imageView = findViewById(R.id.ts02_imgView);

		btnExit.setOnClickListener(this);
		zoomControls.setOnZoomInClickListener(zoonInListener);
		zoomControls.setOnZoomOutClickListener(zoonOutListener);

		reDrawView();
		setBitmap();
	}

	private void reDrawView(){
		float x = imageView.getScaleX();
		float y = imageView.getScaleY();

		imageView.setScaleX((float) (x - 0.0));
		imageView.setScaleY((float) (y - 0.0));
	}

	private void setBitmap(){
		int topicId = getIntent().getExtras().getInt(Constants.TOPIC_NAME);
		switch (topicId){
			case Constants.ITEM_BB_CAM:
				txtTitleBar.setText("BIỂN BÁO CẤM");
				imageView.setBackground(getResources().getDrawable(R.drawable.bb1_cam01));
				break;
			case Constants.ITEM_BB_NGUY_HIEM:
				txtTitleBar.setText("BIỂN BÁO NGUY HIỂM");
				imageView.setBackground(getResources().getDrawable(R.drawable.bb2_nguy_hiem01));
				break;
			case Constants.ITEM_BB_HIEU_LENH:
				txtTitleBar.setText("BIỂN HIỆU LỆNH");
				imageView.setBackground(getResources().getDrawable(R.drawable.bb3_hieu_lenh01));
				break;
			case Constants.ITEM_BB_CHI_DAN:
				txtTitleBar.setText("BIỂN CHỈ DẪN");
				imageView.setBackground(getResources().getDrawable(R.drawable.bb4_chi_dan01));
				break;
			case Constants.ITEM_BB_PHU:
				txtTitleBar.setText("BIỂN PHỤ");
				imageView.setBackground(getResources().getDrawable(R.drawable.bb5_bao_phu01));
				break;
			case Constants.ITEM_BB_VACH_KE_DUONG:
				txtTitleBar.setText("VẠCH KẺ ĐƯỜNG");
				imageView.setBackground(getResources().getDrawable(R.drawable.bb6_vach_ke_duong01));
				break;
		}
	}

	@Override
	public void onBackPressed() {
		finish();
	}

    @Override
    public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ts02_btnExit:
				imageView.setBackground(getResources().getDrawable(R.color.white));
				SignDetailActivity.this.finish();
				break;
		}
    }

	View.OnClickListener zoonInListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			float x = imageView.getScaleX();
			float y = imageView.getScaleY();

			if (x < 1 && y < 1) {
				imageView.setScaleX((float) (x + 0.05));
				imageView.setScaleY((float) (y + 0.05));
			} else {
				zoomControls.setIsZoomInEnabled(false);
			}
			zoomControls.setIsZoomOutEnabled(true);
		}
	};

	View.OnClickListener zoonOutListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			float x = imageView.getScaleX();
			float y = imageView.getScaleY();

			if (x > 0.2 && y > 0.2) {
				imageView.setScaleX((float) (x - 0.05));
				imageView.setScaleY((float) (y - 0.05));
			} else {
				zoomControls.setIsZoomOutEnabled(false);
			}
			zoomControls.setIsZoomInEnabled(true);
		}
	};
}