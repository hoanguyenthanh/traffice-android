package com.htn.traffic.activitys;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.htn.traffic.R;
import com.htn.traffic.UIApplication;
import com.htn.traffic.helpers.Constants;
import com.htn.traffic.helpers.SFSetting;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created date: 13/12/2016
 */
public class StudyActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener{
	public static final String TAG = "StudyActivity";
	private ConstraintLayout linearBottom;

	private Spinner mSpinner;

	private ImageView btnBack;
    private ImageView btnNext;
    private ImageView btnPrev;
	private ImageView btnAnswer;
	private ImageView btnMark;

	private List<Question> listQuestion;
	private int sizeQuestion = 0;
	private int mCurPosition = 0;
	private int topicId = 150;

	private ViewPager mViewPager;
	private QuestionStudyAdapter questionStudyAdapter;
	private SFSetting sfSetting;


	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mViewPager.addOnPageChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mViewPager.removeOnPageChangeListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.study_layout);

		sfSetting = new SFSetting(this);

		btnBack = findViewById(R.id.sd01_btnBack);
		btnMark = findViewById(R.id.sd01_btnMark);
		btnNext = findViewById(R.id.sd01_btnNext);
		btnPrev = findViewById(R.id.sd01_btnPrev);
		btnAnswer = findViewById(R.id.sd01_btnAnswer);
		mViewPager = findViewById(R.id.sd01_viewpager);
		mSpinner = findViewById(R.id.sd01_spinner);
		linearBottom = findViewById(R.id.sd01_lnerButtom);

		setQuestByTopic();
		setSpinner();
		setLasQuest();

		loadDrawableAnswer();

		questionStudyAdapter = new QuestionStudyAdapter(listQuestion, getSupportFragmentManager());
		mViewPager.setAdapter(questionStudyAdapter);

		mSpinner.setOnItemSelectedListener(onItemSelectedListener);
		btnBack.setOnClickListener(this);
		btnMark.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnPrev.setOnClickListener(this);
		btnAnswer.setOnClickListener(this);
	}

	private void setQuestByTopic(){
		Bundle intent = getIntent().getExtras();

		topicId =  intent.getInt(Constants.TOPIC_ID);
		String screen = intent.getString(Constants.SCREEN_ID);

		if (Constants.CAR_LOW_ACTIVITY.equals(screen)) {
			if (topicId == Constants.TOPIC_450) {
				listQuestion = UIApplication.getQuestionDAL().getQuestionsAll(Constants.TOPIC_450);
			}
			else if (topicId == Constants.TOPIC_REVIEW_450){
				listQuestion = UIApplication.getQuestionDAL().getQuestionsMarked(Constants.TOPIC_450);
			}
			else{
				listQuestion = UIApplication.getQuestionDAL().getQuestions(topicId, Constants.TOPIC_450);
			}
		}
		else if (Constants.MOTORBIKE_LOW_ACTIVITY.equals(screen)) {
			if (topicId == Constants.TOPIC_150){
				listQuestion = UIApplication.getQuestionDAL().getQuestionsAll(Constants.TOPIC_150);
			}
			else if (topicId == Constants.TOPIC_REVIEW_150){
				listQuestion = UIApplication.getQuestionDAL().getQuestionsMarked(Constants.TOPIC_150);
			}
			else{
				listQuestion = UIApplication.getQuestionDAL().getQuestions(topicId, Constants.TOPIC_150);
			}
		}

		if (listQuestion != null) {
			for (Question question : listQuestion) {
				question.setListChoice();
			}
			sizeQuestion = listQuestion.size();
		}

		if (sizeQuestion == 0){
			linearBottom.setVisibility(View.INVISIBLE);
		}
	}

	private void setSpinner() {
		List<String> sList = new ArrayList<>();
		for (int i = 1; i <= sizeQuestion; i++ ){
			String label = "Câu " + String.valueOf(i);
			sList.add(label);
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.question_spinner_holder, sList);
		adapter.setDropDownViewResource(R.layout.drop_down_spinner_layout);

		mSpinner.setAdapter(adapter);
	}

	private void saveLastQuest(){
		int curItem = mViewPager.getCurrentItem();
		switch (topicId){
			case Constants.TOPIC_150:
				UIApplication.getHistoryStudyDAL().setLastIndexItem150(curItem);
				break;
			case Constants.TOPIC_450:
				UIApplication.getHistoryStudyDAL().setLastIndexItem450(curItem);
				break;
			case Constants.TOPIC_01:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic01(curItem);
				break;
			case Constants.TOPIC_02:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic02(curItem);
				break;
			case Constants.TOPIC_03:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic03(curItem);
				break;
			case Constants.TOPIC_04:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic04(curItem);
				break;
			case Constants.TOPIC_05:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic05(curItem);
				break;
			case Constants.TOPIC_06:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic06(curItem);
				break;
			case Constants.TOPIC_07:
				UIApplication.getHistoryStudyDAL().setLastIndexTopic07(curItem);
				break;
		}
	}


	private void setLasQuest(){
		if (listQuestion.size() > 0) {
			int indexItem = 0;
			switch (topicId){
				case Constants.TOPIC_150:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexItem150();
					break;
				case Constants.TOPIC_450:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexItem450();
					break;
				case Constants.TOPIC_01:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic01();
					break;
				case Constants.TOPIC_02:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic02();
					break;
				case Constants.TOPIC_03:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic03();
					break;
				case Constants.TOPIC_04:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic04();
					break;
				case Constants.TOPIC_05:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic05();
					break;
				case Constants.TOPIC_06:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic06();
					break;
				case Constants.TOPIC_07:
					indexItem = UIApplication.getHistoryStudyDAL().getLastIndexTopic07();
					break;
			}

			if (listQuestion.size() - indexItem > 0) {
				mViewPager.setCurrentItem(indexItem);
			}
		}
	}

	public void loadDrawableAnswer(){
		boolean show = sfSetting.getShowAnswer();
		Drawable drawable = show ? getResources().getDrawable(R.drawable.ic_view) : getResources().getDrawable(R.drawable.ic_unview);
		btnAnswer.setBackground(drawable);
	}

	public void loadDrawableSave(Question question){
		Drawable drawable = question.isMark == 1 ? getResources().getDrawable(R.drawable.ic_save)
				: getResources().getDrawable(R.drawable.ic_unsave);
		btnMark.setBackground(drawable);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.sd01_btnBack:
				saveLastQuest();
				mViewPager.removeAllViews();
				mViewPager.destroyDrawingCache();
				getSupportFragmentManager().popBackStackImmediate();
				finish();
				break;
			case R.id.sd01_btnMark:
				Question question = listQuestion.get(mViewPager.getCurrentItem());
				int mark = (question.isMark == 0 ? 1 : 0);

				question.setIsMark(mark);
				boolean update = UIApplication.getQuestionDAL().updateQuestMarked(question.questId, mark) > 0;

				String message = (update && mark == 0) ?  "Đã xóa khỏi mục đánh dấu" : "Đã thêm vào mục Đánh dấu";
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

				loadDrawableSave(question);
				break;
			case R.id.sd01_btnNext:
				mCurPosition = mViewPager.getCurrentItem();
				if (mCurPosition < sizeQuestion - 1){
					mViewPager.setCurrentItem(mCurPosition + 1);
				}
				break;
			case R.id.sd01_btnPrev:
				mCurPosition = mViewPager.getCurrentItem();
				if (mCurPosition > 0){
					mViewPager.setCurrentItem(mCurPosition - 1);
				}
				break;
			case R.id.sd01_btnAnswer:
				boolean show = !sfSetting.getShowAnswer();
				sfSetting.setShowAnswer(show);
				loadDrawableAnswer();

				QuestionContentFragment fragment = questionStudyAdapter.getFragment(mCurPosition);
				if (fragment != null){
					fragment.showAnswer(show, listQuestion.get(mCurPosition));
				}

				String message1 = show ? "Hiện đáp án" : "Ẩn đáp án";
				Toast.makeText(this, message1, Toast.LENGTH_SHORT).show();
				break;
		}
	}


	private AdapterView.OnItemSelectedListener onItemSelectedListener =  new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			mViewPager.setCurrentItem(position, true);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}
	};

	@Override
	public void onPageSelected(int position) {
		Log.d(TAG, "onPageSelected: position = " + position);
		mCurPosition = position;
		mSpinner.setSelection(mCurPosition);

		Question question = listQuestion.get(position);
		boolean showAnswer = sfSetting.getShowAnswer();

		loadDrawableSave(question);
		loadDrawableAnswer();

		QuestionContentFragment fragment = questionStudyAdapter.getFragment(position);
		if (fragment != null){
			fragment.showAnswer(showAnswer, question);
		}
	}

	@Override
	public void onPageScrolled(int position, float arg1, int arg2) {
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}



	public static class QuestionStudyAdapter extends FragmentStatePagerAdapter {
		private List<Question> mQuestion;
		private Hashtable<Integer, QuestionContentFragment> mFragments;

		public QuestionStudyAdapter(List<Question> q, FragmentManager fm) {
			super(fm);
			mQuestion = q;
			mFragments = new Hashtable<>();
		}

		@Override
		public int getCount() {
			return mQuestion == null ? 0 : mQuestion.size();
		}

		@Override
		public Fragment getItem(int position) {
			QuestionContentFragment fragment = QuestionContentFragment.newInstance(mQuestion.get(position), position);
			return fragment;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			Object object = super.instantiateItem(container, position);
			if (object instanceof QuestionContentFragment){
				QuestionContentFragment fragment = (QuestionContentFragment) object;
				mFragments.put(position, fragment);
			}

			return object;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			super.destroyItem(container, position, object);
			if (mFragments != null && mFragments.containsKey(position)){
				mFragments.remove(position);
			}
		}

		public QuestionContentFragment getFragment(int position) {
			if (mFragments != null && mFragments.containsKey(position)){
				return mFragments.get(position);
			}
			return null;
		}
	}
}
