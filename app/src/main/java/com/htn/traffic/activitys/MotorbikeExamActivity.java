package com.htn.traffic.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.htn.traffic.R;
import com.htn.traffic.handledata.HistoryStudyDAL;
import com.htn.traffic.helpers.Constants;
import com.htn.traffic.helpers.ExamMotorFactory;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;

import static com.htn.traffic.helpers.Constants.KEY_LEVEL;
import static com.htn.traffic.helpers.Constants.KEY_QUESTS;
import static com.htn.traffic.helpers.Constants.KEY_TITLE;

/**
 * Crated date: 17/12/2016
 * Class name: Menu exam of motorbike
 */
public class MotorbikeExamActivity extends Activity{
    String TAG = MotorbikeExamActivity.class.getSimpleName();

    private Button btnExam1, btnExam2, btnExam3, btnExam4, btnExam5, btnExam6, btnExam7, btnExam8;
    private HistoryStudyDAL historyStudy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_motor_layout);
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        setScoreToButton();
    }

    private void initialize(){
        historyStudy = new HistoryStudyDAL(getApplicationContext());

        Button btnBackHome = findViewById(R.id.mmex0101_btnBackHome);
        Button btnRandom = findViewById(R.id.mmex0101_btnRandom);
        btnExam1 = findViewById(R.id.mmex0101_btnExam1);
        btnExam2 = findViewById(R.id.mmex0101_btnExam2);
        btnExam3 = findViewById(R.id.mmex0101_btnExam3);
        btnExam4 = findViewById(R.id.mmex0101_btnExam4);
        btnExam5 = findViewById(R.id.mmex0101_btnExam5);
        btnExam6 = findViewById(R.id.mmex0101_btnExam6);
        btnExam7 = findViewById(R.id.mmex0101_btnExam7);
        btnExam8 = findViewById(R.id.mmex0101_btnExam8);

        btnBackHome.setOnClickListener(clickListener);
        btnRandom.setOnClickListener(clickListener);
        btnExam1.setOnClickListener(clickListener);
        btnExam2.setOnClickListener(clickListener);
        btnExam3.setOnClickListener(clickListener);
        btnExam4.setOnClickListener(clickListener);
        btnExam5.setOnClickListener(clickListener);
        btnExam6.setOnClickListener(clickListener);
        btnExam7.setOnClickListener(clickListener);
        btnExam8.setOnClickListener(clickListener);
    }

    public void setScoreToButton(){
        int iScore = historyStudy.getScore150Exam01();
        if (iScore >= 16){
            btnExam1.setText(getResultTextPass(1, iScore));
        }else if (iScore > 0){
            btnExam1.setText(getResultTextNotPass(1, iScore));
        }

        iScore = historyStudy.getScore150Exam02();
        if (iScore >= 16){
            btnExam2.setText(getResultTextPass(2, iScore));
        }else if (iScore > 0){
            btnExam2.setText(getResultTextNotPass(2, iScore));
        }

        iScore = historyStudy.getScore150Exam03();
        if (iScore >= 16){
            btnExam3.setText(getResultTextPass(3, iScore));
        }else if (iScore > 0){
            btnExam3.setText(getResultTextNotPass(3, iScore));
        }

        iScore = historyStudy.getScore150Exam04();
        if (iScore >= 16){
            btnExam4.setText(getResultTextPass(4, iScore));
        }else if (iScore > 0){
            btnExam4.setText(getResultTextNotPass(4, iScore));
        }

        iScore = historyStudy.getScore150Exam05();
        if (iScore >= 16){
            btnExam5.setText(getResultTextPass(5, iScore));
        }else if (iScore > 0){
            btnExam5.setText(getResultTextNotPass(5, iScore));
        }

        iScore = historyStudy.getScore150Exam06();
        if (iScore >= 16){
            btnExam6.setText(getResultTextPass(6, iScore));
        }else if (iScore > 0){
            btnExam6.setText(getResultTextNotPass(6, iScore));
        }

        iScore = historyStudy.getScore150Exam07();
        if (iScore >= 16){
            btnExam7.setText(getResultTextPass(7, iScore));
        }else if (iScore > 0){
            btnExam7.setText(getResultTextNotPass(7, iScore));
        }

        iScore = historyStudy.getScore150Exam08();
        if (iScore >= 16){
            btnExam8.setText(getResultTextPass(8, iScore));
        }else if (iScore > 0){
            btnExam8.setText(getResultTextNotPass(8, iScore));
        }
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.mmex0101_btnRandom:
                    startActivityExam(Constants.TOPIC_150, "random");
                    break;
                case R.id.mmex0101_btnExam1:
                    startActivityExam(1, "");
                    break;
                case R.id.mmex0101_btnExam2:
                    startActivityExam(2, "");
                    break;
                case R.id.mmex0101_btnExam3:
                    startActivityExam(3, "");
                    break;
                case R.id.mmex0101_btnExam4:
                    startActivityExam(4, "");
                    break;
                case R.id.mmex0101_btnExam5:
                    startActivityExam(5, "");
                    break;
                case R.id.mmex0101_btnExam6:
                    startActivityExam(6, "");
                    break;
                case R.id.mmex0101_btnExam7:
                    startActivityExam(7, "");
                    break;
                case R.id.mmex0101_btnExam8:
                    startActivityExam(8, "");
                    break;
                case R.id.mmex0101_btnBackHome:
                    finish();
                    break;
            }
        }
    };

    private void startActivityExam(int examId, String typeTest){
        ExamMotorFactory factory = new ExamMotorFactory();
        factory.factoryExam(examId);

        List<Question> listQuest = factory.getListQuest();
        String title = "random".equals(typeTest) ? "Bộ đề ngẫu nhiên" : "Bộ đề " + examId;

        Intent intent = new Intent(getApplicationContext(), ExamActivity.class);
        intent.putParcelableArrayListExtra(KEY_QUESTS, (ArrayList<? extends Parcelable>) listQuest);
        intent.putExtra(KEY_TITLE, title);
        intent.putExtra(KEY_LEVEL, "motor");

        startActivity(intent);
    }

    private String getResultTextPass(int examId, int score){
        return "Bộ đề " + examId + " (Hoàn thành " + score + "/20: Đạt)";
    }

    private String getResultTextNotPass(int examId, int score){
        return "Bộ đề " + examId + " (Hoàn thành " + score + "/20: Chưa đạt)";
    }
}
