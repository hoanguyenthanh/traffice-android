package com.htn.traffic.helpers;

import com.htn.traffic.UIApplication;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DLV-LAP-37 on 16/12/2016.
 */

public class ExamMotorFactory {
    private  final int[] EXAM01_TOPIC01 = {1, 3, 4, 5, 7, 8, 9, 12, 13};
    private  final int[] EXAM02_TOPIC01 = {14, 16, 17, 19, 22, 24, 25, 26, 27};
    private  final int[] EXAM03_TOPIC01 = {28, 29, 30, 31, 32, 35, 36, 38, 39};
    private  final int[] EXAM04_TOPIC01 = {45, 46, 47, 48, 50, 52, 53, 54, 55};
    private  final int[] EXAM05_TOPIC01 = {56, 57, 58, 59, 60, 61, 65, 67, 68};
    private  final int[] EXAM06_TOPIC01 = {70, 71, 80, 83, 84, 85, 86, 87, 88};
    private  final int[] EXAM07_TOPIC01 = {89, 91, 92, 97, 98, 99, 100, 101, 102};
    private  final int[] EXAM08_TOPIC01 = {103, 105, 112, 113, 117, 126, 127, 130, 131};

    private  final int[] EXAM01_TOPIC03 = {133};
    private  final int[] EXAM02_TOPIC03 = {134};
    private  final int[] EXAM03_TOPIC03 = {136};
    private  final int[] EXAM04_TOPIC03 = {186};
    private  final int[] EXAM05_TOPIC03 = {188};
    private  final int[] EXAM06_TOPIC03 = {193};
    private  final int[] EXAM07_TOPIC03 = {194};
    private  final int[] EXAM08_TOPIC03 = {197};

    private  final int[] EXAM01_TOPIC06 = {256, 257, 258, 262, 263};
    private  final int[] EXAM02_TOPIC06 = {264, 265, 266, 268, 269};
    private  final int[] EXAM03_TOPIC06 = {270, 271, 274, 275, 276};
    private  final int[] EXAM04_TOPIC06 = {277, 278, 279, 280, 281};
    private  final int[] EXAM05_TOPIC06 = {282, 283, 284, 285, 289};
    private  final int[] EXAM06_TOPIC06 = {290, 291, 298, 312, 324};
    private  final int[] EXAM07_TOPIC06 = {325, 345, 347, 349, 351};
    private  final int[] EXAM08_TOPIC06 = {256, 264, 270, 277, 282}; //Get again 5 quest

    private  final int[] EXAM01_TOPIC07 = {356, 359, 361, 362, 364};
    private  final int[] EXAM02_TOPIC07 = {368, 369, 372, 373, 374};
    private  final int[] EXAM03_TOPIC07 = {375, 376, 377, 378, 379};
    private  final int[] EXAM04_TOPIC07 = {386, 389, 394, 396, 397};
    private  final int[] EXAM05_TOPIC07 = {398, 407, 408, 409, 412};
    private  final int[] EXAM06_TOPIC07 = {417, 422, 425, 428, 429};
    private  final int[] EXAM07_TOPIC07 = {430, 431, 434, 436, 437};
    private  final int[] EXAM08_TOPIC07 = {356, 368, 375, 386, 398}; //Get again 5 quest

    private List<Question> listQuest = new ArrayList<Question>();
    private int[] arrayQuestId;
    private  int descStart = 0;

    public List<Question> getListQuest(){
        return listQuest;
    }

    private void addArrayToArray(int[] source){
        System.arraycopy(source, 0, arrayQuestId, descStart, source.length);
        descStart += source.length;
    }

    private void setQuestionRandom(){
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_01, Constants.TOPIC_150, 9));
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_03, Constants.TOPIC_150, 1));
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_06, Constants.TOPIC_150, 5));
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_07, Constants.TOPIC_150, 5));
    }

    public void factoryExam(int examId){
        if (examId == Constants.TOPIC_150){
            setQuestionRandom();
        }else {
            descStart = 0;
            arrayQuestId = new int[20];
            switch (examId){
                case 1:
                    addArrayToArray(EXAM01_TOPIC01);
                    addArrayToArray(EXAM01_TOPIC03);
                    addArrayToArray(EXAM01_TOPIC06);
                    addArrayToArray(EXAM01_TOPIC07);
                    break;
                case 2:
                    addArrayToArray(EXAM02_TOPIC01);
                    addArrayToArray(EXAM02_TOPIC03);
                    addArrayToArray(EXAM02_TOPIC06);
                    addArrayToArray(EXAM02_TOPIC07);
                    break;
                case 3:
                    addArrayToArray(EXAM03_TOPIC01);
                    addArrayToArray(EXAM03_TOPIC03);
                    addArrayToArray(EXAM03_TOPIC06);
                    addArrayToArray(EXAM03_TOPIC07);
                    break;
                case 4:
                    addArrayToArray(EXAM04_TOPIC01);
                    addArrayToArray(EXAM04_TOPIC03);
                    addArrayToArray(EXAM04_TOPIC06);
                    addArrayToArray(EXAM04_TOPIC07);
                    break;
                case 5:
                    addArrayToArray(EXAM05_TOPIC01);
                    addArrayToArray(EXAM05_TOPIC03);
                    addArrayToArray(EXAM05_TOPIC06);
                    addArrayToArray(EXAM05_TOPIC07);
                    break;
                case 6:
                    addArrayToArray(EXAM06_TOPIC01);
                    addArrayToArray(EXAM06_TOPIC03);
                    addArrayToArray(EXAM06_TOPIC06);
                    addArrayToArray(EXAM06_TOPIC07);
                    break;
                case 7:
                    addArrayToArray(EXAM07_TOPIC01);
                    addArrayToArray(EXAM07_TOPIC03);
                    addArrayToArray(EXAM07_TOPIC06);
                    addArrayToArray(EXAM07_TOPIC07);
                    break;
                case 8:
                    addArrayToArray(EXAM08_TOPIC01);
                    addArrayToArray(EXAM08_TOPIC03);
                    addArrayToArray(EXAM08_TOPIC06);
                    addArrayToArray(EXAM08_TOPIC07);
                    break;
            }
            listQuest = UIApplication.getQuestionDAL().getQuestions(arrayQuestId,  Constants.TOPIC_150);
        }
    }

}
