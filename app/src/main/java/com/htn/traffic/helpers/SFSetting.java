package com.htn.traffic.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class SFSetting {
    public Context context;
    private SharedPreferences preferences;

    public static final String KEY_IS_SHOW_ANSWER = "KEY_IS_SHOW_ANSWER";


    public SFSetting (Context context){
        this.context = context;
        preferences = context.getSharedPreferences("SF_TRAFFIC_SETTINGS", Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences(){
        return this.preferences;
    }

    public boolean getShowAnswer(){
        return this.preferences.getBoolean(KEY_IS_SHOW_ANSWER, false);
    }


    public void setShowAnswer(boolean show){
        this.preferences.edit().putBoolean(KEY_IS_SHOW_ANSWER, show).apply();
    }
}
