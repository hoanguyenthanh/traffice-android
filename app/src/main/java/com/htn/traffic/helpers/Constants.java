package com.htn.traffic.helpers;

/**
 * Created by HOA on 23/04/2016.
 */
public class Constants {
    public static final String TOPIC_NAME = "TOPIC_NAME";
    public static final String TOPIC_ID = "TOPIC_ID";
    public static final String SCREEN_ID = "SCREEN_ID";
    public static final String MOTORBIKE_LOW_ACTIVITY = "motorbike_car_activit";
    public static final String CAR_LOW_ACTIVITY = "car_low_activity";
    public static final String EXAM_RESULT_ACTIVITY = "ResultExamActivity";

    public static final String LEVEL_CAR = "car";
    public static final String LEVEL_MOTOR = "motor";

    public static int SCORE_PAST_CAR = 28;
    public static int SCORE_PAST_MOTOR = 16;

    public static int TIME_EXAM_CAR = 30;
    public static int TIME_EXAM_MOTOR = 15;


    public static final String KEY_TITLE = "key_title";
    public static final String KEY_QUESTS = "key_quests";
    public static final String KEY_LEVEL = "key_level";
    public static final String KEY_RULE_PAST = "key_rule_past";
    public static final String KEY_EXAM_ID = "key_exam_id";


    public static final int TOPIC_150 = 150;
    public static final int TOPIC_450 = 450;
    public static final int TOPIC_REVIEW_150 = 1150;
    public static final int TOPIC_REVIEW_450 = 1450;
    /**
     * Khái niệm và quy tắc giao thông đường bộ
     */
    public static final int TOPIC_01 = 1;
    /**
     * Nghiệp vụ vận tải
     */
    public static final int TOPIC_02 = 2;
    /**
     * Văn hóa, đạo đức nghề nghiệp người lái xe
     */
    public static final int TOPIC_03 = 3;
    /**
     * Kỹ thuật lái xe ô tô
     */
    public static final int TOPIC_04 = 4;
    /**
     * Cấu tạo và sửa chữa xe ô tô
     */
    public static final int TOPIC_05 = 5;
    /**
     * Hệ thống biển báo hiệu đường bộ
     */
    public static final int TOPIC_06 = 6;
    /**
     * Giải các thế sa hình
     */
    public static final int TOPIC_07 = 7;

    /**
     * 0
     */
    public static final int ITEM_BB_CAM = 0;
    /**
     * 1
     */
    public static final int ITEM_BB_NGUY_HIEM = 1;
    /**
     * 2
     */
    public static final int ITEM_BB_HIEU_LENH = 2;
    /**
     * 3
     */
    public static final int ITEM_BB_CHI_DAN = 3;
    /**
     * 4
     */
    public static final int ITEM_BB_PHU = 4;
    /**
     * 5
     */
    public static final int ITEM_BB_VACH_KE_DUONG = 5;
}
