package com.htn.traffic.helpers;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class Utilities {

	public static String convertMultiValues2String(int[] values) {
		StringBuffer buff = new StringBuffer();
		buff.append("(");

		if(values.length > 0){
			buff.append(values[0]);
		}

		for(int i = 1; i < values.length; i++){
			buff.append(",");
			buff.append(values[i]);
		}

		buff.append(")");
		return buff.toString();
	}

	public static String convertMultiValues2String(Collection<Integer> values) {
		StringBuffer buff = new StringBuffer();

		buff.append("(");

		Iterator<Integer> iterator = values.iterator();
		if (iterator.hasNext()) {
			buff.append(iterator.next());
		}
		while (iterator.hasNext()) {
			buff.append(",");
			buff.append(iterator.next());
		}

		buff.append(")");

		return buff.toString();
	}

	public static String getCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:main_layout:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static long getCurrentDateTimeInLong() {
		Date date = new Date();
		return date.getTime();
	}

	public static String getFormatResultDuration(long duration) {
		int hour = (int) (duration / 3600);
		int min = (int) ((duration % 3600) / 60);
		int sec = (int) ((duration % 3600) % 60);
		StringBuffer buffer = new StringBuffer();

		if (hour > 0) {
			buffer.append(hour);
			buffer.append("h ");
		}

		if (min > 0) {
			buffer.append(min);
			buffer.append("m ");
		}

		if (sec > 0) {
			buffer.append(sec);
			buffer.append("s");
		}

		return buffer.toString();
	}

	public static String getFormatCountDownTimerDuration(long seconds) {
		int hour = (int) (seconds / 3600);
		int min = (int) (seconds / 60);
		int sec = (int) (seconds % 60);
		StringBuffer buffer = new StringBuffer();

		if (hour > 0) {
			if (hour < 10) {
				buffer.append("0");
			}
			buffer.append(hour);
			buffer.append(":");
		}

		if (min < 10) {
			buffer.append("0");
		}
		buffer.append(min);
		buffer.append(":");

		if (sec < 10) {
			buffer.append("0");
		}
		buffer.append(sec);

		return buffer.toString();
	}

	/**
	 * @param strURL
	 *            Dia chi web dan toi file tren internet
	 * @return Data Stream trang thai ket noi: thanh cong: 200
	 *         (HttpStatus.SC_OK) => !NULL that bai 404
	 *         (HttpStatus.SC_NOT_FOUND) => NULL
	 */
	public static InputStream checkFileAvailableStatus(String strURL) {

		InputStream xmlStream = null;
		HttpURLConnection httpConn = null;

		try {
			URL url = new URL(strURL);
			URLConnection connection = url.openConnection();

			if (connection instanceof HttpURLConnection) {
				httpConn = (HttpURLConnection) connection;
				httpConn.setDoInput(true);
				httpConn.setRequestProperty("charset", "utf-8");
				httpConn.setConnectTimeout(30 * 1000); // 30s

				if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					xmlStream = httpConn.getInputStream();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (httpConn != null){
				httpConn.disconnect();
			}
		}
		return xmlStream;
	}

	/**
	 * @param activity
	 * @return internet connection status of the device
	 */
	public boolean isInternetConnected(Activity activity) {
		ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}
}
