package com.htn.traffic.helpers;

import com.htn.traffic.UIApplication;
import com.htn.traffic.models.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DLV-LAP-37 on 16/12/2016.
 *
 * List topic
 * 1. Khái niệm và quy tắc giao thông đường bộ
 * 2. Nghiệp vụ vận tải
 * 3. Văn hóa, đạo đức nghề nghiệp người lái xe
 * 4. Kỹ thuật lái xe ô tô
 * 5. Cấu tạo và sửa chữa xe ô tô
 * 6. Hệ thống biển báo hiệu đường bộ
 * 7. Giải các thế sa hình
 */

public class ExamCarFactory {
    private final int[] EXAM01_TOPIC02 = {146, 147};
    private final int[] EXAM02_TOPIC02 = {148, 149};
    private final int[] EXAM03_TOPIC02 = {150, 151};
    private final int[] EXAM04_TOPIC02 = {152, 153};
    private final int[] EXAM05_TOPIC02 = {154, 155};
    private final int[] EXAM06_TOPIC02 = {156, 157};
    private final int[] EXAM07_TOPIC02 = {158, 159};
    private final int[] EXAM08_TOPIC02 = {160, 161};
    private final int[] EXAM09_TOPIC02 = {162, 163};
    private final int[] EXAM10_TOPIC02 = {164, 165};
    private final int[] EXAM11_TOPIC02 = {166, 167};
    private final int[] EXAM12_TOPIC02 = {168, 169};
    private final int[] EXAM13_TOPIC02 = {170, 171};
    private final int[] EXAM14_TOPIC02 = {172, 173};
    private final int[] EXAM15_TOPIC02 = {174, 175};


    //EXAM01_TOPIC03 + EXAM01_TOPIC01_KN = 3
    private final int[] EXAM01_TOPIC03 = {176};
    private final int[] EXAM02_TOPIC03 = {177};
    private final int[] EXAM03_TOPIC03 = {178};
    private final int[] EXAM04_TOPIC03 = {179};
    private final int[] EXAM05_TOPIC03 = {180};
    private final int[] EXAM06_TOPIC03 = {181, 182};
    private final int[] EXAM07_TOPIC03 = {183, 184};
    private final int[] EXAM08_TOPIC03 = {185, 186};
    private final int[] EXAM09_TOPIC03 = {187, 188};
    private final int[] EXAM10_TOPIC03 = {189, 190};
    private final int[] EXAM11_TOPIC03 = {191, 192};
    private final int[] EXAM12_TOPIC03 = {193, 194};
    private final int[] EXAM13_TOPIC03 = {195, 196};
    private final int[] EXAM14_TOPIC03 = {197, 198};
    private final int[] EXAM15_TOPIC03 = {199, 200};

    private final int[] EXAM01_TOPIC01_KN = {1, 2};
    private final int[] EXAM02_TOPIC01_KN = {3, 4};
    private final int[] EXAM03_TOPIC01_KN = {5, 6};
    private final int[] EXAM04_TOPIC01_KN = {7, 8};
    private final int[] EXAM05_TOPIC01_KN = {9, 10};
    private final int[] EXAM06_TOPIC01_KN = {11};
    private final int[] EXAM07_TOPIC01_KN = {12};
    private final int[] EXAM08_TOPIC01_KN = {13};
    private final int[] EXAM09_TOPIC01_KN = {14};
    private final int[] EXAM10_TOPIC01_KN = {15};
    private final int[] EXAM11_TOPIC01_KN = {16};
    private final int[] EXAM12_TOPIC01_KN = {17};
    private final int[] EXAM13_TOPIC01_KN = {18};
    private final int[] EXAM14_TOPIC01_KN = {19};
    private final int[] EXAM15_TOPIC01_KN = {20};


    private final int[] EXAM01_TOPIC01_TD = {21};
    private final int[] EXAM02_TOPIC01_TD = {132};
    private final int[] EXAM03_TOPIC01_TD = {133};
    private final int[] EXAM04_TOPIC01_TD = {134};
    private final int[] EXAM05_TOPIC01_TD = {135};
    private final int[] EXAM06_TOPIC01_TD = {136};
    private final int[] EXAM07_TOPIC01_TD = {137};
    private final int[] EXAM08_TOPIC01_TD = {138};
    private final int[] EXAM09_TOPIC01_TD = {139};
    private final int[] EXAM10_TOPIC01_TD = {140};
    private final int[] EXAM11_TOPIC01_TD = {141};
    private final int[] EXAM12_TOPIC01_TD = {142};
    private final int[] EXAM13_TOPIC01_TD = {143};
    private final int[] EXAM14_TOPIC01_TD = {144};
    private final int[] EXAM15_TOPIC01_TD = {145};


    //EXAM01_TOPIC01_QT + EXAM01_TOPIC04 = 11
    private final int[] EXAM01_TOPIC01_QT = {22, 23, 24, 25, 26, 27, 28};
    private final int[] EXAM02_TOPIC01_QT = {29, 30, 31, 32, 33, 34, 35};
    private final int[] EXAM03_TOPIC01_QT = {36, 37, 38, 39, 40, 41, 42};
    private final int[] EXAM04_TOPIC01_QT = {43, 44, 45, 46, 47, 48, 49};
    private final int[] EXAM05_TOPIC01_QT = {50, 51, 52, 53, 54, 55, 56};
    private final int[] EXAM06_TOPIC01_QT = {57, 58, 59, 60, 61, 62, 63};
    private final int[] EXAM07_TOPIC01_QT = {64, 65, 66, 67, 68, 69, 70};
    private final int[] EXAM08_TOPIC01_QT = {71, 72, 73, 74, 75, 76, 77};
    private final int[] EXAM09_TOPIC01_QT = {78, 79, 80, 81, 82, 83, 84};
    private final int[] EXAM10_TOPIC01_QT = {85, 86, 87, 88, 89, 90, 91};
    private final int[] EXAM11_TOPIC01_QT = {92, 93, 94, 95, 96, 97, 98, 99};
    private final int[] EXAM12_TOPIC01_QT = {100, 101, 102, 103, 104, 105, 106, 107};
    private final int[] EXAM13_TOPIC01_QT = {108, 109, 110, 111, 112, 113, 114, 115};
    private final int[] EXAM14_TOPIC01_QT = {116, 117, 118, 119, 120, 121, 122, 123};
    private final int[] EXAM15_TOPIC01_QT = {124, 125, 126, 127, 128, 129, 130, 131};

    private final int[] EXAM01_TOPIC045 = {201, 202, 203, 304};
    private final int[] EXAM02_TOPIC045 = {205, 206, 207, 208};
    private final int[] EXAM03_TOPIC045 = {209, 210, 211, 212};
    private final int[] EXAM04_TOPIC045 = {213, 214, 215, 216};
    private final int[] EXAM05_TOPIC045 = {217, 218, 219, 220};
    private final int[] EXAM06_TOPIC045 = {221, 222, 223, 224};
    private final int[] EXAM07_TOPIC045 = {225, 226, 227, 228};
    private final int[] EXAM08_TOPIC045 = {229, 230, 231, 232};
    private final int[] EXAM09_TOPIC045 = {233, 234, 235, 236};
    private final int[] EXAM10_TOPIC045 = {237, 238, 239, 240};
    private final int[] EXAM11_TOPIC045 = {241, 242, 243};
    private final int[] EXAM12_TOPIC045 = {244, 245, 246};
    private final int[] EXAM13_TOPIC045 = {247, 248, 249};
    private final int[] EXAM14_TOPIC045 = {250, 251, 252};
    private final int[] EXAM15_TOPIC045 = {253, 254, 255};


    //EXAM01_TOPIC06 + EXAM01_TOPIC07 = 13
    private final int[] EXAM01_TOPIC06 = {256, 257, 258, 259, 260, 261, 262};
    private final int[] EXAM02_TOPIC06 = {263, 264, 265, 266, 267, 268, 269};
    private final int[] EXAM03_TOPIC06 = {270, 271, 272, 273, 274, 275, 276};
    private final int[] EXAM04_TOPIC06 = {277, 278, 279, 280, 281, 282, 283};
    private final int[] EXAM05_TOPIC06 = {284, 285, 286, 287, 288, 289, 290};
    private final int[] EXAM06_TOPIC06 = {291, 292, 293, 294, 295, 296, 297};
    private final int[] EXAM07_TOPIC06 = {298, 299, 300, 301, 302, 303, 304};
    private final int[] EXAM08_TOPIC06 = {305, 306, 307, 308, 309, 310, 311};
    private final int[] EXAM09_TOPIC06 = {312, 313, 314, 315, 316, 317, 318};
    private final int[] EXAM10_TOPIC06 = {319, 320, 321, 322, 323, 324, 325};
    private final int[] EXAM11_TOPIC06 = {326, 327, 328, 329, 330, 331};
    private final int[] EXAM12_TOPIC06 = {332, 333, 334, 335, 336, 337};
    private final int[] EXAM13_TOPIC06 = {338, 339, 340, 341, 342, 343};
    private final int[] EXAM14_TOPIC06 = {344, 345, 346, 347, 348, 349};
    private final int[] EXAM15_TOPIC06 = {350, 351, 352, 353, 354, 355};

    private final int[] EXAM01_TOPIC07 = {356, 357, 358, 359, 360, 361};
    private final int[] EXAM02_TOPIC07 = {362, 363, 364, 365, 366, 367};
    private final int[] EXAM03_TOPIC07 = {368, 369, 370, 371, 372, 373};
    private final int[] EXAM04_TOPIC07 = {374, 375, 376, 377, 378, 379};
    private final int[] EXAM05_TOPIC07 = {380, 381, 382, 383, 384, 385};
    private final int[] EXAM06_TOPIC07 = {386, 387, 388, 389, 390, 391};
    private final int[] EXAM07_TOPIC07 = {392, 393, 394, 395, 396, 397};
    private final int[] EXAM08_TOPIC07 = {398, 399, 400, 401, 402, 403};
    private final int[] EXAM09_TOPIC07 = {404, 405, 406, 407, 408, 409};
    private final int[] EXAM10_TOPIC07 = {410, 411, 412, 413, 414, 415};
    private final int[] EXAM11_TOPIC07 = {416, 417, 418, 419, 420, 421, 422};
    private final int[] EXAM12_TOPIC07 = {423, 424, 425, 426, 427, 428, 429};
    private final int[] EXAM13_TOPIC07 = {430, 431, 432, 433, 434, 435, 436};
    private final int[] EXAM14_TOPIC07 = {437, 438, 439, 440, 441, 442, 443};
    private final int[] EXAM15_TOPIC07 = {444, 445, 446, 447, 448, 449, 450};

    private List<Question> listQuest = new ArrayList<Question>();
    private int[] arrayQuestId;
    private int descStart = 0;

    public List<Question> getListQuest(){
        return listQuest;
    }

    private void addArrayToArray(int[] source){
        System.arraycopy(source, 0, arrayQuestId, descStart, source.length);
        descStart += source.length;
    }

    /**
     * Get list quest by examID
     */

    private void setQuestionRandom(){
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_01, Constants.TOPIC_450, 9));
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_02, Constants.TOPIC_450, 1));
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_03, Constants.TOPIC_450, 1));
        if (Math.random() <= 0.5) {
            listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_04, Constants.TOPIC_450, 1));
        }else {

            listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_05, Constants.TOPIC_450, 1));
        }
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_06, Constants.TOPIC_450, 9));
        listQuest.addAll(UIApplication.getQuestionDAL().getQuestionsRandom(Constants.TOPIC_07, Constants.TOPIC_450, 9));
    }

    public void factoryExam(int examId){
        //450 is random
        if (examId == Constants.TOPIC_450){
            setQuestionRandom();
        }else {
            descStart = 0;
            arrayQuestId = new int[30];

            switch (examId) {
                case 1:
                    addArrayToArray(EXAM01_TOPIC01_KN);
                    addArrayToArray(EXAM01_TOPIC01_QT);
                    addArrayToArray(EXAM01_TOPIC01_TD);
                    addArrayToArray(EXAM01_TOPIC02);
                    addArrayToArray(EXAM01_TOPIC03);
                    addArrayToArray(EXAM01_TOPIC045);
                    addArrayToArray(EXAM01_TOPIC06);
                    addArrayToArray(EXAM01_TOPIC07);
                    break;
                case 2:
                    addArrayToArray(EXAM02_TOPIC01_KN);
                    addArrayToArray(EXAM02_TOPIC01_QT);
                    addArrayToArray(EXAM02_TOPIC01_TD);
                    addArrayToArray(EXAM02_TOPIC02);
                    addArrayToArray(EXAM02_TOPIC03);
                    addArrayToArray(EXAM02_TOPIC045);
                    addArrayToArray(EXAM02_TOPIC06);
                    addArrayToArray(EXAM02_TOPIC07);
                    break;
                case 3:
                    addArrayToArray(EXAM03_TOPIC01_KN);
                    addArrayToArray(EXAM03_TOPIC01_QT);
                    addArrayToArray(EXAM03_TOPIC01_TD);
                    addArrayToArray(EXAM03_TOPIC02);
                    addArrayToArray(EXAM03_TOPIC03);
                    addArrayToArray(EXAM03_TOPIC045);
                    addArrayToArray(EXAM03_TOPIC06);
                    addArrayToArray(EXAM03_TOPIC07);
                    break;
                case 4:
                    addArrayToArray(EXAM04_TOPIC01_KN);
                    addArrayToArray(EXAM04_TOPIC01_QT);
                    addArrayToArray(EXAM04_TOPIC01_TD);
                    addArrayToArray(EXAM04_TOPIC02);
                    addArrayToArray(EXAM04_TOPIC03);
                    addArrayToArray(EXAM04_TOPIC045);
                    addArrayToArray(EXAM04_TOPIC06);
                    addArrayToArray(EXAM04_TOPIC07);
                    break;
                case 5:
                    addArrayToArray(EXAM05_TOPIC01_KN);
                    addArrayToArray(EXAM05_TOPIC01_QT);
                    addArrayToArray(EXAM05_TOPIC01_TD);
                    addArrayToArray(EXAM05_TOPIC02);
                    addArrayToArray(EXAM05_TOPIC03);
                    addArrayToArray(EXAM05_TOPIC045);
                    addArrayToArray(EXAM05_TOPIC06);
                    addArrayToArray(EXAM05_TOPIC07);
                    break;
                case 6:
                    addArrayToArray(EXAM06_TOPIC01_KN);
                    addArrayToArray(EXAM06_TOPIC01_QT);
                    addArrayToArray(EXAM06_TOPIC01_TD);
                    addArrayToArray(EXAM06_TOPIC02);
                    addArrayToArray(EXAM06_TOPIC03);
                    addArrayToArray(EXAM06_TOPIC045);
                    addArrayToArray(EXAM06_TOPIC06);
                    addArrayToArray(EXAM06_TOPIC07);
                    break;
                case 7:
                    addArrayToArray(EXAM07_TOPIC01_KN);
                    addArrayToArray(EXAM07_TOPIC01_QT);
                    addArrayToArray(EXAM07_TOPIC01_TD);
                    addArrayToArray(EXAM07_TOPIC02);
                    addArrayToArray(EXAM07_TOPIC03);
                    addArrayToArray(EXAM07_TOPIC045);
                    addArrayToArray(EXAM07_TOPIC06);
                    addArrayToArray(EXAM07_TOPIC07);
                    break;
                case 8:
                    addArrayToArray(EXAM08_TOPIC01_KN);
                    addArrayToArray(EXAM08_TOPIC01_QT);
                    addArrayToArray(EXAM08_TOPIC01_TD);
                    addArrayToArray(EXAM08_TOPIC02);
                    addArrayToArray(EXAM08_TOPIC03);
                    addArrayToArray(EXAM08_TOPIC045);
                    addArrayToArray(EXAM08_TOPIC06);
                    addArrayToArray(EXAM08_TOPIC07);
                    break;
                case 9:
                    addArrayToArray(EXAM09_TOPIC01_KN);
                    addArrayToArray(EXAM09_TOPIC01_QT);
                    addArrayToArray(EXAM09_TOPIC01_TD);
                    addArrayToArray(EXAM09_TOPIC02);
                    addArrayToArray(EXAM09_TOPIC03);
                    addArrayToArray(EXAM09_TOPIC045);
                    addArrayToArray(EXAM09_TOPIC06);
                    addArrayToArray(EXAM09_TOPIC07);
                    break;
                case 10:
                    addArrayToArray(EXAM10_TOPIC01_KN);
                    addArrayToArray(EXAM10_TOPIC01_QT);
                    addArrayToArray(EXAM10_TOPIC01_TD);
                    addArrayToArray(EXAM10_TOPIC02);
                    addArrayToArray(EXAM10_TOPIC03);
                    addArrayToArray(EXAM10_TOPIC045);
                    addArrayToArray(EXAM10_TOPIC06);
                    addArrayToArray(EXAM10_TOPIC07);
                    break;
                case 11:
                    addArrayToArray(EXAM11_TOPIC01_KN);
                    addArrayToArray(EXAM11_TOPIC01_QT);
                    addArrayToArray(EXAM11_TOPIC01_TD);
                    addArrayToArray(EXAM11_TOPIC02);
                    addArrayToArray(EXAM11_TOPIC03);
                    addArrayToArray(EXAM11_TOPIC045);
                    addArrayToArray(EXAM11_TOPIC06);
                    addArrayToArray(EXAM11_TOPIC07);
                    break;
                case 12:
                    addArrayToArray(EXAM12_TOPIC01_KN);
                    addArrayToArray(EXAM12_TOPIC01_QT);
                    addArrayToArray(EXAM12_TOPIC01_TD);
                    addArrayToArray(EXAM12_TOPIC02);
                    addArrayToArray(EXAM12_TOPIC03);
                    addArrayToArray(EXAM12_TOPIC045);
                    addArrayToArray(EXAM12_TOPIC06);
                    addArrayToArray(EXAM12_TOPIC07);
                    break;
                case 13:
                    addArrayToArray(EXAM13_TOPIC01_KN);
                    addArrayToArray(EXAM13_TOPIC01_QT);
                    addArrayToArray(EXAM13_TOPIC01_TD);
                    addArrayToArray(EXAM13_TOPIC02);
                    addArrayToArray(EXAM13_TOPIC03);
                    addArrayToArray(EXAM13_TOPIC045);
                    addArrayToArray(EXAM13_TOPIC06);
                    addArrayToArray(EXAM13_TOPIC07);
                    break;
                case 14:
                    addArrayToArray(EXAM14_TOPIC01_KN);
                    addArrayToArray(EXAM14_TOPIC01_QT);
                    addArrayToArray(EXAM14_TOPIC01_TD);
                    addArrayToArray(EXAM14_TOPIC02);
                    addArrayToArray(EXAM14_TOPIC03);
                    addArrayToArray(EXAM14_TOPIC045);
                    addArrayToArray(EXAM14_TOPIC06);
                    addArrayToArray(EXAM14_TOPIC07);
                    break;
                case 15:
                    addArrayToArray(EXAM15_TOPIC01_KN);
                    addArrayToArray(EXAM15_TOPIC01_QT);
                    addArrayToArray(EXAM15_TOPIC01_TD);
                    addArrayToArray(EXAM15_TOPIC02);
                    addArrayToArray(EXAM15_TOPIC03);
                    addArrayToArray(EXAM15_TOPIC045);
                    addArrayToArray(EXAM15_TOPIC06);
                    addArrayToArray(EXAM15_TOPIC07);
                    break;
            }

            listQuest = UIApplication.getQuestionDAL().getQuestions(arrayQuestId,  Constants.TOPIC_450);
        }
    }
}
