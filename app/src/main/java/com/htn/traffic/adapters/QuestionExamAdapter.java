package com.htn.traffic.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.UIApplication;
import com.htn.traffic.models.Choice;
import com.htn.traffic.models.Question;

import java.util.List;

/**
 * Created date: 14/12/2016
 * By: HoaNguyen
 * Class name:
 */
public class QuestionExamAdapter extends PagerAdapter {
    public static final String TAG = "QuestionExamAdapter";
	private AppCompatActivity mContext;
	private LayoutInflater inflater;
	private List<Question> mQuestion;
	private View.OnClickListener mOnClickListener;

	public QuestionExamAdapter(AppCompatActivity context, List<Question> q, View.OnClickListener listener) {
		mContext = context;
		mQuestion = q;
        mOnClickListener = listener;
        inflater = (LayoutInflater)  context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return mQuestion == null ? 0 : mQuestion.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.question_exam_holder, container, false);

		Question question = mQuestion.get(position);
		List<Choice> choiceList = question.getListChoice();

		TextView txtQNum = layout.findViewById(R.id.ex0201_txtQNum);
		TextView txtTitle = layout.findViewById(R.id.ex0201_txtTitle);
		ImageView imageView = layout.findViewById(R.id.ex0201_image);

		TextView txtQ1 = layout.findViewById(R.id.ex0201_txtQuest1);
		TextView txtQ2 = layout.findViewById(R.id.ex0201_txtQuest2);
		TextView txtQ3 = layout.findViewById(R.id.ex0201_txtQuest3);
		TextView txtQ4 = layout.findViewById(R.id.ex0201_txtQuest4);

		Button btnAns1 = layout.findViewById(R.id.ex0201_btnAns1);
		Button btnAns2 = layout.findViewById(R.id.ex0201_btnAns2);
		Button btnAns3 = layout.findViewById(R.id.ex0201_btnAns3);
		Button btnAns4 = layout.findViewById(R.id.ex0201_btnAns4);

		LinearLayout linearAns3 = layout.findViewById(R.id.ex0201_linearAns3);
		LinearLayout linearAns4 = layout.findViewById(R.id.ex0201_linearAns4);

		txtQ3.setVisibility(View.GONE);
		txtQ4.setVisibility(View.GONE);
		linearAns3.setVisibility(View.INVISIBLE);
		linearAns4.setVisibility(View.INVISIBLE);


		txtQNum.setText("Câu " + (position + 1) + ":");
		txtTitle.setText(question.title);

		Drawable tick = mContext.getResources().getDrawable(R.drawable.ic_tick);
		Drawable unTick = mContext.getResources().getDrawable(R.drawable.ic_untick);

		int index = 0;
		for (Choice choice : choiceList) {
			switch (index){
				case 0:
					txtQ1.setText(Html.fromHtml(choice.getTitle().trim()));
					if (question.getValueAnswer(0) == 1){
						btnAns1.setBackground(tick);
					}else{
						btnAns1.setBackground(unTick);
					}

					break;
				case 1:
					txtQ2.setText(Html.fromHtml(choice.getTitle().trim()));
					if (question.getValueAnswer(1) == 1){
						btnAns2.setBackground(tick);
					}else{
						btnAns2.setBackground(unTick);
					}
					break;
				case 2:
					txtQ3.setVisibility(View.VISIBLE);
					txtQ3.setText(Html.fromHtml(choice.getTitle().trim()));

					linearAns3.setVisibility(View.VISIBLE);
					if (question.getValueAnswer(2) == 1){
						btnAns3.setBackground(tick);
					}else{
						btnAns3.setBackground(unTick);
					}

					break;
				case 3:
					txtQ4.setVisibility(View.VISIBLE);
					txtQ4.setText(Html.fromHtml(choice.getTitle().trim()));

					linearAns3.setVisibility(View.VISIBLE);
					if (question.getValueAnswer(3) == 1){
						btnAns4.setBackground(tick);
					}else{
						btnAns4.setBackground(unTick);
					}
					break;
			}
			index++;
		}

		btnAns1.setOnClickListener(mOnClickListener);
		btnAns2.setOnClickListener(mOnClickListener);
		btnAns3.setOnClickListener(mOnClickListener);
		btnAns4.setOnClickListener(mOnClickListener);

		Drawable drawable = getDrawable(question.image);
		if(drawable != null){
			imageView.setImageDrawable(drawable);
		}else{
			imageView.setVisibility(View.GONE);
		}

		container.addView(layout);

		return layout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}

	public Drawable getDrawable(String name) {
		Drawable drawable = null;
		int sourceId = mContext.getResources().getIdentifier(name, "drawable", mContext.getPackageName());
		if (sourceId > 0){
			try {
				drawable = mContext.getResources().getDrawable(sourceId);
			}catch (Resources.NotFoundException e){
			}
		}
		return drawable;
	}
}
