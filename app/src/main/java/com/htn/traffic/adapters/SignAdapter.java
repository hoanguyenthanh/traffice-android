package com.htn.traffic.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.helpers.Constants;

public class SignAdapter extends BaseAdapter {
	private Context mContext;
	private String[] mListItem;
	private LayoutInflater inflater;

	public SignAdapter(Context context, String[] list) {
		mContext = context;
		mListItem = list;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		return mListItem.length;
	}

	@Override
	public Object getItem(int pos) {
		return mListItem[pos];
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView = inflater.inflate(R.layout.sign_holder, null);
		}

		ImageView imgView = convertView.findViewById(R.id.ts0101_image);
		TextView txtTitle = convertView.findViewById(R.id.ts0101_txtTitle);

		txtTitle.setText(mListItem[pos]);


		Drawable drawable = getDrawable(pos);
		if(drawable != null){
			imgView.setImageDrawable(drawable);
		}

		return convertView;
	}

	private Drawable getDrawable(int position){
		Drawable drawable = null;
		int sourceID = 0;

		switch (position){
			case Constants.ITEM_BB_CAM:
				sourceID = mContext.getResources().getIdentifier("bb_cam", "drawable", mContext.getPackageName());
				break;
			case Constants.ITEM_BB_NGUY_HIEM:
				sourceID = mContext.getResources().getIdentifier("bb_nguyhiem", "drawable", mContext.getPackageName());
				break;
			case Constants.ITEM_BB_HIEU_LENH:
				sourceID = mContext.getResources().getIdentifier("bb_hieulenh", "drawable", mContext.getPackageName());
				break;
			case Constants.ITEM_BB_CHI_DAN:
				sourceID = mContext.getResources().getIdentifier("bb_chidan", "drawable", mContext.getPackageName());
				break;
			case Constants.ITEM_BB_PHU:
				sourceID = mContext.getResources().getIdentifier("bb_bienphu", "drawable", mContext.getPackageName());
				break;
			case Constants.ITEM_BB_VACH_KE_DUONG:
				sourceID = mContext.getResources().getIdentifier("bb_vachkeduong", "drawable", mContext.getPackageName());
				break;
		}

		if (sourceID > 0){
			try{
				drawable = mContext.getResources().getDrawable(sourceID);
			}catch (Resources.NotFoundException e){
				drawable = null;
			}
		}

		return drawable;
	}
}