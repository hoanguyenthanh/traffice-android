package com.htn.traffic.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.models.Choice;
import com.htn.traffic.models.Question;

import java.util.Collection;
import java.util.List;

/**
 * Created date: 14/12/2016
 * By: HoaNguyen
 * Class name:
 */
public class ResultExamAdapter extends BaseAdapter{
	private Context mContext;
	private LayoutInflater inflater;
	private List<Question> mQuestion;
	private StringBuilder sYourAnswer = new StringBuilder();
	private StringBuilder sAnswerCorrect = new StringBuilder();

	public ResultExamAdapter(Context c, List<Question> q) {
		mContext = c;
		inflater = (LayoutInflater) c.getSystemService(c.LAYOUT_INFLATER_SERVICE);
		mQuestion = q;
	}
	
	@Override
	public int getCount() {
		return mQuestion.size();
	}

	@Override
	public Object getItem(int position) {
		return mQuestion.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mQuestion.get(position).questId;
	}

	@Override
	public View getView(int position, View layout, ViewGroup parent) {
		layout = inflater.inflate(R.layout.exam_result_holder, null);

		sAnswerCorrect.setLength(0);
		sYourAnswer.setLength(0);
		Question question = mQuestion.get(position);
		Collection<Choice> choiceList = question.getListChoice();

		TextView txtQNum = layout.findViewById(R.id.ex0301_txtQNum);
		TextView txtTrueFalse = layout.findViewById(R.id.ex0301_txtTrueFalse);

		TextView txtTitle = layout.findViewById(R.id.ex0301_txtTitle);
		ImageView imageView = layout.findViewById(R.id.ex0301_image);
		TextView txtQ1 = layout.findViewById(R.id.ex0301_txtQuest1);
		TextView txtQ2 = layout.findViewById(R.id.ex0301_txtQuest2);
		TextView txtQ3 = layout.findViewById(R.id.ex0301_txtQuest3);
		TextView txtQ4 = layout.findViewById(R.id.ex0301_txtQuest4);

		TextView txtYourAnswer = layout.findViewById(R.id.ex0301_txtYourAnswer);
		TextView txtAnswerCorrect = layout.findViewById(R.id.ex0301_txtAnswerCorrect);

		txtQ3.setVisibility(View.GONE);
		txtQ4.setVisibility(View.GONE);

		txtQNum.setText("Câu " + (position + 1) + ":");
		txtTitle.setText(question.title);

		int index = 0;
		for (Choice choice : choiceList) {
			switch (index){
				case 0:
					txtQ1.setText(Html.fromHtml(choice.getTitle().trim()));

					setsYourAnswer(0, question.getValueAnswer(0));
					setsAnswerCorrect(0, choice.getIsCorrect());
					break;
				case 1:
					txtQ2.setText(Html.fromHtml(choice.getTitle().trim()));

					setsYourAnswer(1, question.getValueAnswer(1));
					setsAnswerCorrect(1, choice.getIsCorrect());
					break;
				case 2:
					txtQ3.setVisibility(View.VISIBLE);
					txtQ3.setText(Html.fromHtml(choice.getTitle().trim()));

					setsYourAnswer(2, question.getValueAnswer(2));
					setsAnswerCorrect(2, choice.getIsCorrect());
					break;
				case 3:
					txtQ4.setVisibility(View.VISIBLE);
					txtQ4.setText(Html.fromHtml(choice.getTitle().trim()));

					setsYourAnswer(3, question.getValueAnswer(3));
					setsAnswerCorrect(3, choice.getIsCorrect());
					break;
			}
			index++;
		}

		txtYourAnswer.setText(sYourAnswer.toString());
		txtAnswerCorrect.setText(sAnswerCorrect.toString());
		if (mQuestion.get(position).trueFalse){
			txtTrueFalse.setText("Đúng");
			txtTrueFalse.setTextColor(mContext.getResources().getColor(R.color.green_light));
		}else{
			txtTrueFalse.setText("Sai");
			txtTrueFalse.setTextColor(mContext.getResources().getColor(R.color.red2));
		}

		Drawable drawable = getDrawable(question.image);
		if(drawable != null){
			imageView.setVisibility(View.VISIBLE);
			imageView.setImageDrawable(drawable);
		}else{
			imageView.setVisibility(View.GONE);
		}
		return layout;
	}

	private void setsYourAnswer(int numAns, int ans){
		if (ans == 1){
			if (sYourAnswer.length() == 0){
				sYourAnswer.append(Integer.toString(numAns + 1));
			}else{
				sYourAnswer.append(", " + Integer.toString(numAns + 1));
			}
		}
	}

	private void setsAnswerCorrect(int numAns, int ans){
		if (ans == 1){
			if (sAnswerCorrect.length() == 0){
				sAnswerCorrect.append(Integer.toString(numAns + 1));
			}else{
				sAnswerCorrect.append(", " + Integer.toString(numAns + 1));
			}
		}
	}

	public Drawable getDrawable(String name) {
		Drawable drawable = null;
		int sourceId = mContext.getResources().getIdentifier(name, "drawable", mContext.getPackageName());
		if (sourceId > 0){
			try {
				drawable = mContext.getResources().getDrawable(sourceId);
			}catch (Resources.NotFoundException e){
				drawable = null;
			}
		}
		return drawable;
	}
}
