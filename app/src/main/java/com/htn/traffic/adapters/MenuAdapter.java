package com.htn.traffic.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.htn.traffic.R;
import com.htn.traffic.activitys.MainActivity;
import com.htn.traffic.models.MenuItem;

import java.util.List;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewMenuHolder> {
    private Context context;
    private List<MenuItem> menuItemsList;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(MenuItem item);
    }

	public MenuAdapter(List<MenuItem> items, OnItemClickListener listener){
	    this.menuItemsList = items;
	    this.onItemClickListener = listener;
    }

    @Override
    public ViewMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
	    context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_holder, parent,false);
        return new ViewMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewMenuHolder holder, int position) {

	    final MenuItem menuItems = this.menuItemsList.get(position);

        holder.txtTitle.setText(menuItems.value);
        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null){
                    onItemClickListener.onItemClick(menuItems);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.menuItemsList == null ? 0 : this.menuItemsList.size();
    }

    public static class ViewMenuHolder extends RecyclerView.ViewHolder{
        TextView txtTitle;

        public ViewMenuHolder(View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.mm_item_menu_txtTitle);
        }
    }


    protected Drawable getIcon(String key){
        Drawable drawable = null;
        switch (key){
            case MainActivity.ITEM_MOTOR_EXAM:
                drawable = context.getResources().getDrawable(R.drawable.moto);
                break;
            case MainActivity.ITEM_MOTOR_LOW:
                drawable = context.getResources().getDrawable(R.drawable.learn_moto);
                break;
            case MainActivity.ITEM_CAR_EXAM:
                drawable = context.getResources().getDrawable(R.drawable.oto);
                break;
            case MainActivity.ITEM_CAR_LOW:
                drawable = context.getResources().getDrawable(R.drawable.learn_oto);
                break;
            case MainActivity.ITEM_SIGNS:
                drawable = context.getResources().getDrawable(R.drawable.trafficsign);
                break;
        }

        return drawable;
    }
}