package com.htn.traffic.models;

public class Choice {
	private int id;
	private String title;
	private int isCorrect;
	private int question_id;

	public Choice() {
	}

	public Choice(int id, String title, int isCorrect, int question_id) {
		this.id = id;
		this.title = title;
		this.isCorrect = isCorrect;
		this.question_id = question_id;
	}

	public boolean isCorrect()
	{
		return isCorrect > 0;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getIsCorrect() {
		return isCorrect;
	}
	public void setIsCorrect(int isCorrect) {
		this.isCorrect = isCorrect;
	}
	public int getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

}
