package com.htn.traffic.models;


public class Setting {
	private boolean timer = false;
	private boolean hideIsCorrect = false;
	private boolean isRandomQuestion = false;

	public Setting() {
	}

	public boolean getTimer() {
		return timer;
	}

	public void setTimer(boolean timer) {
		this.timer = timer;
	}

	public boolean getHideIsCorrect() {
		return hideIsCorrect;
	}

	public void setHideIsCorrect(boolean hideIsCorrect) {
		this.hideIsCorrect = hideIsCorrect;
	}

	public boolean getRandomQuestion() {
		return isRandomQuestion;
	}

	public void setRandomQuestion(boolean isRandomQuestion) {
		this.isRandomQuestion = isRandomQuestion;
	}
}
