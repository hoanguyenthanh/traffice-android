package com.htn.traffic;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.htn.traffic.handledata.ChoiceDAL;
import com.htn.traffic.handledata.HistoryStudyDAL;
import com.htn.traffic.handledata.QuestionDAL;
import com.htn.traffic.handledata.SettingDAL;
import com.htn.traffic.handledata.TopicDAL;
import com.htn.traffic.handledata.xmlhandle.DBHelper;
import com.htn.traffic.handledata.xmlhandle.ImportData;
import com.htn.traffic.models.Topic;

import java.util.Collection;

public class UIApplication extends Application {
	private static Context context;
	private static SQLiteDatabase db;
	private static DBHelper dbHelper;

	private static TopicDAL topicDAL;
	private static QuestionDAL questionDAL;
	private static ChoiceDAL choiceDAL;
	private static ImportData commonDAL;
	private static SettingDAL settingDAL;
	private static HistoryStudyDAL thistoryStudyDAL;

	/**
	 * Tra ve DB dang ket noi
	 * 
	 * @return SQLiteDB
	 */
	public static SQLiteDatabase getConnection() {
		return db;
	}
	public static TopicDAL getTopicDAL() {
		return topicDAL;
	}
	public static QuestionDAL getQuestionDAL() {
		return questionDAL;
	}
	public static ChoiceDAL getChoiceDAL() {
		return choiceDAL;
	}
	public static ImportData getCommonDAL() {
		return commonDAL;
	}
	public static SettingDAL getSettingDAL() {
		return settingDAL;
	}
	public static HistoryStudyDAL getHistoryStudyDAL() {
		return thistoryStudyDAL;
	}
	public static Context getContext() {
		return context;
	}

	/**
	 * @return all topics in DB 
	 */
	public static Collection<Topic> getTopics() {
		return getTopicDAL().getTopics();
	}


	@Override
	public void onCreate() {
		super.onCreate();
		context = this.getApplicationContext();

		dbHelper = new DBHelper(this);
		dbHelper.open();
		db = dbHelper.getConnection();

		settingDAL = new SettingDAL(context);
		thistoryStudyDAL = new HistoryStudyDAL(context);
		commonDAL = new ImportData(db);
		topicDAL = new TopicDAL(db);
		questionDAL = new QuestionDAL(db);
		choiceDAL = new ChoiceDAL(db);

		UIApplication.getCommonDAL().importDataFromResource();
	}

	@Override
	public void onTerminate() {
		if (dbHelper != null)
			dbHelper.close();

		super.onTerminate();
	}
}